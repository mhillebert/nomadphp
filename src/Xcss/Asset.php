<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Xcss;
/**
 * Class asset
 *
 * @package Nomad\Xcss
 * @author  Mark Hillebert
 */
class asset
{
	/**
	 * @var \Nomad\Core\View
	 */
	protected $_theme;

	/**
	 * @param $theme
	 */
	public function __construct($theme)
	{
		$this->_theme = $theme;
	}

	/**
	 * @param $image
	 * @return string
	 */
	public function image($image)
	{
		return "url(/assets/images/{$image})";
	}
}

?>