<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Xcss;
/**
 * Class color
 *
 * @package Nomad\Xcss
 * @author  Mark Hillebert
 */
class color
{
	/**
	 * Shades a color
	 *
	 * @param       $hex
	 * @param int   $factor
	 * @param float $alpha
	 * @return string
	 */
	public function shade($hex, $factor = 30, $alpha = 1.0)
	{
		$hex = ltrim($hex, '#');

		$rgb = $this->_hexToRgb($hex);
		$hsl = $this->_rgbToHsl($rgb);

		$hsl['s'] = round(($hsl['s'] - $hsl['s'] / 5), 3); //subtract an arbitrary 20% -- looks about right for me
		$hsl['l'] += round(($factor / 100), 2);

		$rgb = $this->_hslToRgb($hsl);

		if ($alpha < 1) {
			return "rgba(" . implode(", ", $rgb) . ", {$alpha})"; // returns the rgb values separated by commas
		}
		$hex = $this->_rgbToHex($rgb);

		return '#' . $hex;
	}

	/**
	 * Return a hex color in rgba format with given opacity
	 * @param $color
	 * @param $opacity
	 * @return string
	 */
	public function rgba($color, $opacity)
	{
		return "rgba(" . implode(',', $this->_hexToRgb($color)) . ", {$opacity})";
	}

	/**
	 * Rotates a color around the color wheel
	 *
	 * @param       $hexColor
	 * @param int   $degrees
	 * @param float $alpha
	 * @return string
	 */
	public function rotate($hexColor, $degrees = 180, $alpha = 1.0)
	{
		$hex = ltrim($hexColor, '#');

		$rgb = $this->_hexToRgb($hex);
		$hsl = $this->_rgbToHsl($rgb);

		$hsl['h'] = abs(($hsl['h'] + $degrees) % 360);

		$rgb = $this->_hslToRgb($hsl);

		if ($alpha < 1) {
			return "rgba(" . implode(", ", $rgb) . ", {$alpha})"; // returns the rgb values separated by commas
		}
		$hex = $this->_rgbToHex($rgb);

		return '#' . $hex;
	}

	/**
	 * @param $hexString
	 * @return mixed
	 */
	public function _hexToRgb($hexString)
	{
		$hex = ltrim($hexString, '#');

		if (strlen($hex) == 3) {
			$base['r'] = hexdec($hex{0} . $hex{0});
			$base['g'] = hexdec($hex{1} . $hex{1});
			$base['b'] = hexdec($hex{2} . $hex{2});
		}
		else {
			$base['r'] = hexdec($hex{0} . $hex{1});
			$base['g'] = hexdec($hex{2} . $hex{3});
			$base['b'] = hexdec($hex{4} . $hex{5});
		}

		return $base;
	}

	/**
	 * @param $rgbArray
	 * @return string
	 */
	public function _rgbToHex($rgbArray)
	{
		$hex = "";

		foreach ($rgbArray as $k => $v) {
			$new_hex_component = dechex($v > 255 ? 255 : ($v < 0 ? 0 : $v));
			if (strlen($new_hex_component) < 2) {
				$new_hex_component = "0" . $new_hex_component;
			}
			$hex .= $new_hex_component;
		}

		return $hex;
	}

	/**
	 * @param $hslArray
	 * @return array
	 */
	public function _hslToRgb($hslArray)
	{
		$h = $hslArray['h'];
		$s = $hslArray['s'] > 1 ? 1 : ($hslArray['s'] < 0 ? 0 : $hslArray['s']);
		$l = $hslArray['l'] > 1 ? 1 : ($hslArray['l'] < 0 ? 0 : $hslArray['l']);

		$c = (1 - abs(2 * $l - 1)) * $s;
		$x = $c * (1 - abs(fmod(($h / 60), 2) - 1));
		$m = $l - ($c / 2);

		if ($h < 60) {
			$r = $c;
			$g = $x;
			$b = 0;
		}
		else {
			if ($h < 120) {
				$r = $x;
				$g = $c;
				$b = 0;
			}
			else {
				if ($h < 180) {
					$r = 0;
					$g = $c;
					$b = $x;
				}
				else {
					if ($h < 240) {
						$r = 0;
						$g = $x;
						$b = $c;
					}
					else {
						if ($h < 300) {
							$r = $x;
							$g = 0;
							$b = $c;
						}
						else {
							$r = $c;
							$g = 0;
							$b = $x;
						}
					}
				}
			}
		}

		$r = ($r + $m) * 255;
		$g = ($g + $m) * 255;
		$b = ($b + $m) * 255;

		return array('r' => floor($r), 'g' => floor($g), 'b' => floor($b));
	}

	/**
	 * @param $rgbArray
	 * @return array
	 */
	public function _rgbToHsl($rgbArray)
	{
		$r = $rgbArray['r'];
		$g = $rgbArray['g'];
		$b = $rgbArray['b'];

		$r /= 255;
		$g /= 255;
		$b /= 255;

		$max = max($r, $g, $b);
		$min = min($r, $g, $b);

		$h = 0;
		$l = ($max + $min) / 2;
		$d = $max - $min;

		if ($d == 0) {
			$h = $s = 0; // achromatic
		}
		else {
			$s = $d / (1 - abs(2 * $l - 1));

			switch ($max) {
				case $r:
					$h = 60 * fmod((($g - $b) / $d), 6);
					if ($b > $g) {
						$h += 360;
					}
					break;

				case $g:
					$h = 60 * (($b - $r) / $d + 2);
					break;

				case $b:
					$h = 60 * (($r - $g) / $d + 4);
					break;
			}
		}

		return array('h' => round($h, 2), 's' => round($s, 2), 'l' => round($l, 2));
	}
}

?>