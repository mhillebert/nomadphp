<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Filter;
/**
 * Class AbstractFilter
 *
 * @package Nomad\Filter
 * @author  Mark Hillebert
 */
abstract class AbstractFilter
	implements Filter_Interface
{
	/**
	 * @var array
	 */
	protected $_params;

	/**
	 * Set any params to their corresponding protected property
	 *
	 * @param       $value
	 * @param array $params
	 */
	public function __construct($value, $params = array())
	{
		if (!empty($params)) {
			foreach ($params as $name => $value) {
				$property = "_{$name}";
				if (property_exists($this, $property)) {
					$this->$property = $value;
				}
			}
		}
	}

	/**
	 * @param       $value
	 * @param array $formValues
	 * @return mixed
	 */
	abstract function filter($value, $formValues = array());
}