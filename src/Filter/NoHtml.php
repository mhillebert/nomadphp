<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Filter;
/**
 * Class NoHtml
 *
 * @package Nomad\Filter
 * @author  Mark Hillebert
 */
class NoHtml
	extends AbstractFilter
{
	const NO_HTML = 'no html';
	const DEFAULT_HTML = 'default';
	const FULL_HTML = 'full';

	/**
	 * @var string
	 */
	protected $_filterStrength;

	/**
	 * @var array
	 */
	protected $_allowedTags = array();

	/**
	 * @var array
	 */
	protected $_allowedAttributes = array();

	/**
	 * Filters valid html tags and no events
	 *
	 * @param       $value
	 * @param array $formValues
	 * @return bool|mixed
	 */
	public function filter($value, $formValues = array())
	{
		$config   = $this->_getConfiguration();
		$purifier = new \HTMLPurifier($config);
		$filtered = $purifier->purify($value);

		return $filtered;
	}

	/**
	 * Sets up the configuration for htmlpurifier
	 *
	 * @return \HTMLPurifier_Config
	 */
	protected function _getConfiguration()
	{
		$config = \HTMLPurifier_Config::createDefault();
		switch ($this->_filterStrength) {
			case self::NO_HTML:
				$config->set('HTML.Allowed', '');
				break;
			case self::FULL_HTML:
				//don't modify htmlpurifier's defaults
				break;
			default: //defaults to self::DEFAULT_HTML
				$config->set('HTML.AllowedElements', implode($this->_allowedTags, ','));
				$config->set('HTML.AllowedAttributes', implode($this->_allowedAttributes, ','));
		}

		return $config;
	}
}