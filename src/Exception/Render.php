<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Exception;
/**
 * Class Render
 *
 * @package Nomad\Exception
 * @author  Mark Hillebert
 */
class Render
	extends \Exception
{
	/**
	 * @var string Default Error Message
	 */
	private static $_message = "No object found to render.";

	/**
	 * Setterer-upperer
	 *
	 * @param null       $message
	 * @param int        $code
	 * @param \Exception $previous
	 */
	public function __construct($message = null, $code = 0, \Exception $previous = null)
	{
		if (!$message) {
			$message = self::$_message;
		}
		parent::__construct($message, $code = 0, $previous = null);
	}
}