<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Exception;
/**
 * Class Annotation
 *
 * @package Nomad\Exception
 * @author  Mark Hillebert
 */
class Annotation
	extends \Exception
{
	/**
	 * @var string Default Error Message
	 */
	private static $_message = "You must use a Nomad Implemented Validator,
                        or use one of 'Namespace\\Class::Static(\$value, \$params)' or 'Namespace\\Class->Method(\$value, \$params)' syntax
                        in the @Nomad\\Validator annotation";

	/**
	 * Setterer-upperer
	 *
	 * @param null       $message
	 * @param int        $code
	 * @param \Exception $previous
	 */
	public function __construct($message = null, $code = 0, \Exception $previous = null)
	{
		if (!$message) {
			$message = self::$_message;
		}
		parent::__construct($message, $code = 0, $previous = null);
	}
}