<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Unify;
/**
 * Class Date
 *
 * @package Nomad\Unify
 * @author  Mark Hillebert
 */
class Date
{
	/**
	 * @const strings Date Formats
	 */
	CONST MYSQL_TIMESTAMP = 'Y-m-d H:i:s';
	CONST MEDIUM_DATETIME = 'M d, y g:ia';
	CONST LONG_DATETIME = 'M d, Y g:ia';
	CONST SHORT_DATE = 'n/j/y';

	public static function MySQL_Timestamp($date, $convertToUTC = false)
	{
		if (is_int($date)) {
			if ($convertToUTC) {
				return gmdate(self::MYSQL_TIMESTAMP, $date);
			}

			$date = date(self::MYSQL_TIMESTAMP, $date);
		}
		else {
			if ($convertToUTC) {
				return gmdate(self::MYSQL_TIMESTAMP, strtotime($date));
			}

			$date = date(self::MYSQL_TIMESTAMP, strtotime($date));
		}

		return $date;
	}

	/**
	 * Short for date format in MEDIUM_DATETIME
	 *
	 * @param $date
	 * @return bool|string
	 */
	public static function FormatMediumDateTime($date)
	{
		return self::DateFormat($date, self::MEDIUM_DATETIME);
	}

	/**
	 * Short for date format in LONG_DATETIME
	 *
	 * @param $date
	 * @return bool|string
	 */
	public static function FormatLongDateTime($date)
	{
		return self::DateFormat($date, self::LONG_DATETIME);
	}

	/**
	 * Formats a given UTC date to current local time with a given format
	 *
	 * @param $date
	 * @param $format
	 * @param $timezone
	 * @return bool|string
	 */
	public static function DateFormat($date, $format, $timezone = null)
	{
		if (is_int($date)) {
			return date($format, $date);
		}
		else {
			$dateObj = new \DateTime($date);
			if ($timezone) {
				$dateObj->setTimezone(new \DateTimeZone($timezone));
			}

			return $dateObj->format($format);
		}
	}

	/**
	 * Returns the '+/-00:00' offset of given php sting timezone
	 *
	 * @param $localTimeZone string
	 * @return string
	 */
	public static function GetOffsetByTimeZone($localTimeZone)
	{
		$time           = new \DateTime(date('Y-m-d H:i:s'), new \DateTimeZone($localTimeZone));
		$timezoneOffset = $time->format('P');

		return $timezoneOffset;
	}

	/**
	 * @param        $dateString
	 * @param string $over6HoursDateFormat
	 * @param string $notTodayDateFormat
	 * @return bool|string
	 */
	public static function Ago($dateString, $over6HoursDateFormat = 'g:ia', $notTodayDateFormat = 'n-j g:ia')
	{
		$timeString = strtotime($dateString);
		if (date('Ymd') == date('Ymd', $timeString)) {
			$secondsAgo = time() - $timeString;
			if ($secondsAgo < 3600) {
				$return = floor($secondsAgo / 60) . ' minutes ago.';
			}
			else {
				$return = date($over6HoursDateFormat, $timeString);
			}
		}
		else {
			$return = date($notTodayDateFormat, $timeString);
		}

		return $return;
	}
}