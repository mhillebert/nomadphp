<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Cache;

use Nomad\Core as Core;
use Nomad\Exception as Exception;

/**
 * Class Driver
 * @TODO Make this abstract with the required functions (set,get, expire, etc)
 * @package Nomad\Cache
 * @author  Mark Hillebert
 */
class Driver
	extends Core\BaseClass
{
	/**
	 * @return $this
	 */
	public function initialize()
	{
		return $this;
	}
}