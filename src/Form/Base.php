<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Form;

use Nomad\Exception as Exception;

/**
 * Class Base
 *
 * @package Nomad\Form
 * @author  Mark Hillebert
 */
class Base
{
	/**
	 * @var string name of form
	 */
	public $name;

	/**
	 * @var string Path of form's page
	 */
	public $action;

	/**
	 * @var string forms method
	 */
	public $method = 'post';

	/**
	 * @var array Form's parameters
	 */
	protected $_params = array();

	/**
	 * @var string Element wrapping decorator
	 */
	public $elementWrapper = 'ul';

	/**
	 * @var array Elements
	 */
	protected $_elements = array();

	/**
	 * @var string name of submit button
	 */
	protected $_submitButtonName;
	/**
	 * @var array invalid Elements
	 */
	protected $_invalidElements;

	/**
	 * @var string path to a custom rendering file. Used to layout a form uniquely
	 */
	public $customRenderFile;

	/**
	 * @var string Html tag to wrap each form error message in.
	 * Note: This is not the elements' html tag wrapper.
	 */
	protected $_formMessageHtmlTag = 'p';
	protected $_formMessageHtmlErrorClass = 'error';

	protected $_generalFormErrorMessage = "Please correct the form below.";

	/**
	 * Determines if Nomad should automatically render the errors near the element.
	 * This can be set on a per element basis too.
	 *
	 * @var bool
	 */
	protected $_useCustomErrorRendering = false;

	/**
	 * @var string|array Error Message for entire form
	 */
	protected $_formErrorMessages;

	/**
	 * functions/methods to execute when passes validation
	 *
	 * @var array
	 */
	protected $_successHandlers = array();

	/**
	 * functions/methods to execute when failed validation
	 *
	 * @var array
	 */
	protected $_failureHandlers = array();

	/**
	 * @var bool
	 */
	protected $_reRenderOnFail = true;

	/**
	 * @var bool
	 */
	protected $_reRenderOnSuccess = false;

	/**
	 * @var bool
	 */
	protected $_hasPassedValidation = false;

	protected $_submitted = false;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Initialize Form
	 */
	public function __construct($name = null, $action = null, $method = 'post')
	{
		$this->name   = $name;
		$this->action = $action;
		$this->method = $method;

		$className   = get_class($this);
		$handlerName = substr_replace($className, '\\Handler', strrpos($className, '\\'), 0);
		if (\file_exists($handlerName)) {
			$this->addFormHandler(new $handlerName());
		}

		$this->init();
	}

	/**
	 * Default function to run
	 */
	public function init()
	{
	}

	/**
	 * Main execution of form.
	 *
	 * @return array|null|string
	 */
	public function handle()
	{
		$return = null;
		if (\Nomad\Core\Session::getSession()->REQUEST_METHOD === 'POST' && isset($_POST[$this->_submitButtonName])) {
			foreach ($this->_elements as $element) {
				$element->setPost(true);
			}
			if ($this->hasValidated()) {
				$this->_hasPassedValidation = true;

				if (!empty($this->_successHandlers)) {
					$return = $this->_runSuccessHandlers();
				}

				if ($this->_reRenderOnSuccess) {
					$return .= $this->render();
				}
			}
			else {

				$this->_hasPassedValidation = false;
				if (!empty($this->_failureHandlers)) {
					$return = $this->_runFailureHandlers();
				}
				if ($this->_reRenderOnFail) {
					$return .= $this->render();
				}
			}
		}
		else {
			$return = $this->render();
		}

		return $return;
	}

	/**
	 * @param boolean $value
	 * @throws \Nomad\Exception\Form
	 */
	public function reRenderOnSuccess($value)
	{
		if (!is_bool($value)) {
			throw new Exception\Form('ReRenderOnSuccess expects a boolean value.');
		}
		$this->_reRenderOnSuccess = $value;
	}

	/**
	 * @param $value
	 * @throws Exception\Form
	 */
	public function reRenderOnFail($value)
	{
		if (!is_bool($value)) {
			throw new Exception\Form('ReRenderOnFail expects a boolean value.');
		}
		$this->_reRenderOnFail = $value;
	}

	/**
	 * adds a function/method to successHandlers array
	 *
	 * @param $funct
	 */
	public function addSuccessHandler($funct)
	{
		//If a Nomad_Form_Handler was added, it contains both a success and failure handler... make sure we add to both.
		if (is_subclass_of($funct, 'Nomad_Form_HandlerAbstract')) {
			$this->_failureHandlers[] = $funct;
		}
		$this->_successHandlers[] = $funct;
	}

	/**
	 * Adds a function/method to failureHandlers array
	 *
	 * @param $funct
	 */
	public function addFailureHandler($funct)
	{
		//If a Nomad_Form_Handler was added, it contains both a success and failure handler... make sure we add to both.
		if (is_subclass_of($funct, 'Nomad_Form_HandlerAbstract')) {
			$this->_successHandlers[] = $funct;
		}
		$this->_failureHandlers[] = $funct;
	}

	/**
	 * Adds a Nomad_Form_Handler to success and failure array.
	 *
	 * @param \Nomad\Form\AbstractHandler $handler
	 * @throws \Nomad\Exception\Form
	 */
	public function addFormHandler(\Nomad\Form\AbstractHandler $handler)
	{
		$this->_failureHandlers[] = $handler;
		$this->_successHandlers[] = $handler;
	}

	/**
	 * Clears all form handlers
	 */
	public function clearFormHandlers()
	{
		$this->_failureHandlers = array();
		$this->_successHandlers = array();
	}

	/**
	 * Execute the methods/functions contains within the successHandler array
	 *
	 * @return array|null|string
	 * @throws Exception\Form
	 */
	protected function _runSuccessHandlers()
	{
		$values = $this->getValues();
		if (!empty($this->_successHandlers)) {
			$success = null;
			foreach ($this->_successHandlers as $successFunction) {
				if (is_object($successFunction)) {
					if (is_subclass_of($successFunction, 'Nomad\Form\AbstractHandler')) {
						$success .= $successFunction->success($this);
					}
					elseif (is_callable($successFunction)) {
						$success .= $successFunction($this);
					}
				}
				elseif (is_array($successFunction)) {
					if (is_object($successFunction[0])) {
						$class = array_shift($successFunction);
						foreach ($successFunction as $method) {
							$success .= $class->{$method}($this);
						}
					}
				}
				else {
					throw new Exception\Form ("Don't understand your success handler.");
				}
			}

			return $success;
		}

		return $values;
	}

	/**
	 * Execute the methods/functions contains within the failureHandler array
	 *
	 * @return array|null|string
	 * @throws Exception\Form
	 */
	protected function _runFailureHandlers()
	{
		$failureHtmlString   = null;
		$failureFormMessages = array();

		if (!empty($this->_failureHandlers)) {

			foreach ($this->_failureHandlers as $failureFunction) {
				if (is_object($failureFunction)) {
					if (is_subclass_of($failureFunction, 'Nomad\Form\AbstractHandler')) {
						$failureFormMessages[] = $failureFunction->failure($this);
					}
					elseif (is_callable($failureFunction)) {
						$failureFormMessages[] = $failureFunction($this);
						//$failure .= $failureFunction($this);
					}
				}
				elseif (is_array($failureFunction)) {
					if (is_object($failureFunction[0])) {
						$class = array_shift($failureFunction);
						foreach ($failureFunction as $method) {
							$failureFormMessages[] = $class->{$method}($this);
							//$failure .= $class->{$method}($this);
						}
					}
				}
				else {
					throw new Exception\Form ("Don't understand your failure handler.");
				}
			}
		}
		/** wrap each message in a tag */
		foreach ($failureFormMessages as $message) {
			// @TODO Put this into a message queue to send to view for rendering
			$failureHtmlString .= "<{$this->_formMessageHtmlTag} class='{$this->_formMessageHtmlErrorClass}'>{$message}</{$this->_formMessageHtmlTag}>";
		}

		return $failureHtmlString;
	}

	/**
	 * Set the elementWrapper
	 *
	 * @param $htmlListElement
	 */
	public function setElementWrapperAs($htmlListElement)
	{
		$this->elementWrapper = $htmlListElement;
	}

	/**
	 * Set the render file
	 *
	 * @param $renderFile
	 */
	public function setCustomRenderer($renderFile)
	{
		$this->customRenderFile = $renderFile;
	}

	/**
	 * Create a new Form Element
	 *
	 * @param       $type
	 * @param       $name
	 * @param array $params
	 * @throws Exception\Form
	 */
	public function createElement($type, $name, array $params = null)
	{
		if (strpos($name, ' ') !== false) {
			throw new Exception\Form("Invalid Form name: '$name'. Cannot contain spaces. Use 'value'=>'your Value' to pass a value.");
		}
		if (!isset($params['useCustomErrorRendering'])) {
			$params['useCustomErrorRendering'] = $this->_useCustomErrorRendering;
		}
		/**
		 * Check for a user created form element class first.
		 * This can (and is allowed) to overwrite nomad classes.
		 * If not found, then try a for a nomad element class, throw exception if no Nomad class exists either
		 */
		$nomadType = "Nomad\\Form\\Element\\" . ucfirst($type);
		if (class_exists($type)) {
			$this->_elements[$name] = new $type ($name, $params);
		}
		elseif (class_exists($nomadType)) {
			$this->_elements[$name] = new $nomadType ($name, $params);
		}
		else {
			throw new Exception\Form('Unknown form element type: ' . $type);
		}
		if ($type == 'submit') {
			$this->_submitButtonName = $name;
		}
	}

	/**
	 * Magic getter for forms --- not sure if i need this anymore
	 *
	 * @param $elementName
	 * @return mixed
	 * @throws Exception\Form
	 */
	public function __get($elementName)
	{
		if (isset($this->_elements[$elementName])) {
			return $this->_elements[$elementName];
		}
		elseif (property_exists($this, '_' . $elementName)) {
			return $this->{'_' . $elementName};
		}

		throw new Exception\Form("Form element '{$elementName}' does not exists.");
	}

	/**
	 * Magic getter for forms --- not sure if i need this anymore
	 *
	 * @param $elementName
	 * @param $value
	 * @return mixed
	 * @throws Exception\Form
	 */
	public function __set($elementName, $value)
	{
		$var = lcfirst(substr($elementName, 3));
		if (!isset($this->_elements[$var])) {
			throw new Exception\Form("Form element '{$var}' does not exists.");
		}
		$this->_elements[$var]->setValue($value);
	}

	/**
	 * Renders the form. Returns the resulting html
	 *
	 * @return string
	 */
	public function render()
	{
		if (isset($this->customRenderFile)) {
			ob_start();
			require($this->customRenderFile);
			$htmlString = ob_get_contents();

			ob_end_clean();
		}
		else {
			$htmlString = "<form name='{$this->name}' " . $this->_createActionString() . " method='{$this->method}'>";
			$htmlString .= "<{$this->elementWrapper}>";
			foreach ($this->_elements as $element) {
				/**
				 * Need to tell the element if its form has been POSTed
				 */

				switch ($this->elementWrapper) {
					case 'ul':
					case 'ol':
						$returnString = "<li id='{$this->name}-item-{$element->getName()}'>" . $element->renderLabel()
							. $element->renderElement() . "</li>";
						break;
					case 'dl':
						$renderedLabel = $element->renderLabel();
						$returnString  = "";
						if ($renderedLabel) {
							$returnString = "<dt id='{$this->name}-label-{$element->getName()}'>" . $element->renderLabel() . "</dt>";
						}
						$returnString .= "<dd id='item-{$element->getName()}'>" . $element->renderElement() . "</dd>";
						break;
					default:
						$returnString = "<{$this->elementWrapper} id='{$this->name}-item-{$element->getName()}'>" . $element->renderLabel()
							. $element->renderElement() . "</{$this->elementWrapper}>";
				}
				$htmlString .= $returnString;
			}
			$htmlString .= "</{$this->elementWrapper}>" . "</form>";
		}

		return $htmlString;
	}

	/**
	 * Set forms action
	 *
	 * @param $action
	 */
	public function setAction($action)
	{
		$this->action = $action;
	}

	/**
	 * Get form's action
	 *
	 * @return string
	 */
	public function getAction()
	{
		return $this->action;
	}

	/**
	 * render helper for forms action
	 *
	 * @return string
	 */
	protected function _createActionString()
	{
		$actionString = "";
		$action       = $this->getAction();
		if (isset($action) && $action != "" && !is_null($action)) {
			$actionString = "action='{$action}'";
		}

		return $actionString;
	}

	/**
	 * @throws Exception\Form
	 * @return bool
	 */
	public function hasValidated()
	{
		$validity = true;
		//element Validation check
		foreach ($this->_elements as $element) {
			if (!$element->passedValidation()) {
				$validity                 = false;
				$this->_invalidElements[] = $element;
			}
		}
		//Form validation check
		if ($validity && isset($this->_params['validators'])) {
			if (is_array($this->_params['validators'])) {
				foreach ($this->_params['validators'] as $validator) {
					//assume the validator is a standard Nomad_Validator class string and not customized
					$type = $validator;
					if (is_array($validator)) { //the validator is customized
						if (!isset($validator['type'])) {
							throw new Exception\Form('Error: ' . $this->_name . ' - Validators "type" must be a key in the validator array.');
						}
						//change the type to the type declared in the validator array
						$type = $validator['type'];
						unset($validator['type']);
					}
					else {
						//because we must pass in an array to the validator as params.
						$validator = array();
					}
					/**
					 * Check for a user created form element class first.
					 * This can (and is allowed) to overwrite nomad classes.
					 * If not found, then try a for a nomad element class, throw exception if no Nomad class exists either
					 */

					$nomadType = "Nomad_Validator_" . ucfirst($type);
					if (class_exists($type)) {
						$validation = new $type($validator);
					}
					elseif (class_exists($nomadType)) {
						$validation = new $nomadType($validator);
					}
					else {
						throw new Exception\Form('Unknown validator type: ' . $type);
					}

					//check for validation and add error message to array (if any)
					if (!$validation->isValid($this->getValues())) {
						$this->_formErrorMessages[] = $validation->getMessage();
						$validity                   = false;
					}
				}
			}
			else {
				throw new Exception\Form('Error: ' . $this->_name . ' - Validators must be an associative array with at least a "type" key.');
			}
		}
		else {
			$this->_formErrorMessages[] = $this->_generalFormErrorMessage;
		}

		return $validity;
	}

	/**
	 * Gets values from form.
	 *
	 *
	 * @return array
	 */
	public function getValues()
	{
		$retObj = array();
		foreach ($this->_elements as $element) {
			if (!$element->ignore) {
				$retObj[$element->getName()] = $element->getValue();
			}
		}

		return $retObj;
	}

	/**
	 * @param $elementName
	 * @return mixed
	 * @throws Exception\Form
	 */
	public function getElement($elementName)
	{
		if (isset($this->_elements[$elementName])) {
			return $this->_elements[$elementName];
		}
		throw new Exception\Form ("Element '{$elementName} does not exist. Cannot get it.");
	}

	/**
	 * @param $value
	 * @return mixed
	 */
	protected function _htmlPropertyFilter($value)
	{
		return (str_replace(" ", "", $value));
	}

	/**
	 * @return array|string
	 */
	public function getFormErrorMessages()
	{
		return $this->_formErrorMessages;
	}
}