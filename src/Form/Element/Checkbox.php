<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Form\Element;

use Nomad\Exception\Form;

/**
 * Class Checkbox
 *
 * @package Nomad\Form\Element
 * @author  Mark Hillebert
 */
class Checkbox
	extends AbstractElement
{
	/**
	 * @var array
	 */
	protected $_options;

	/**
	 * @var string
	 */
	protected $_optionTag;

	/**
	 * @var bool determines whether to show or hide
	 */
	protected $_hideLabels;

	/**
	 * @var string
	 */
	protected $_optionWrapperTag;
	protected $_optionWrapperTagAttributes;

	/**
	 * @var null
	 */
	protected $_originalValue;

	/**
	 * @var string Use this OR optionTag/optionTagWrapper to separate the option;
	 */
	protected $_optionSeparator = '<br/>';

	/**
	 * @param string $name
	 * @param array  $params
	 * @throws Form
	 */
	public function __construct($name, $params = array())
	{
		if (!isset($params['options'])) {
			throw new Form("Radio button must be passed an options array during initialization.");
		}

		$this->_originalValue = isset($params['value']) ? $params['value'] : null;
		parent::__construct($name, $params);
	}

	/**
	 * Renders Element
	 *
	 * @return string
	 */
	public function renderElement()
	{
		$elementHtml = '';
		$nameHtml    = count($this->_options) > 1 ? "name='{$this->_name}[]'" : "name='{$this->_name}'";
		if (empty($this->_options)) {
			$elementHtml = "<input type='checkbox' {$nameHtml} {$this->_requiredHtml} {$this->_attributesHtml}>";
		}
		else {
			foreach ($this->_options as $key => $option) {
				$valueString = "value='{$key}'";
				if (is_array($this->_value)) {
					$selectedString = !in_array($key, $this->_value) ? "" : "checked='checked'";
				}
				else {
					$selectedString = $this->_value != $key ? "" : "checked='checked'";
				}

				$optionHtml     = "<input type='checkbox' {$nameHtml} {$valueString} {$this->_requiredHtml} {$this->_attributesHtml} {$selectedString}>{$option}";
				$usingOptionTag = isset($this->_optionTag);
				if ($usingOptionTag) {
					$wrapperAttributes = isset($this->_optionWrapperTagAttributes) ? " " . $this->_createAttributeString($this->_optionWrapperTagAttributes) : null;
					$optionHtml        = "<{$this->_optionTag}{$wrapperAttributes}>{$optionHtml}</{$this->_optionTag}>";
				}
				else {
					$optionHtml .= $this->_optionSeparator;
				}
				$elementHtml .= $optionHtml;
			}
		}

		if (isset($this->_optionWrapperTag)) {
			$elementHtml = "<{$this->_optionWrapperTag}>{$elementHtml}</{$this->_optionWrapperTag}>";
		}

		return $elementHtml;
	}

	/**
	 * Internal check for isValid.
	 * Make sure the select values is one of the 'set' values and hasn't been altered.
	 *
	 * @param array $formValues
	 * @return bool
	 */
	public function isValid($formValues = array())
	{
		if ($this->_required && is_null($this->getValue())) {
			return false;
		}

		$selectedValue = $this->getValue();
		if (is_array($selectedValue)) {
		}
		else {
			if ((!is_array($selectedValue) && !\array_key_exists($selectedValue, $this->_options))
			) {
				$this->_errorMessages[] = "Invalid selection.";
				$this->_value           = $this->_originalValue;

				return false;
			}
		}

		return true;
	}
}