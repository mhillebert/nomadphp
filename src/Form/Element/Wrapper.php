<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Form\Element;
/**
 * Class Wrapper
 *
 * @package Nomad\Form\Element
 * @author  Mark Hillebert
 */
class Wrapper
	extends AbstractElement
{
	/**
	 * @var bool
	 */
	public $ignore = true;

	/**
	 * @var string
	 */
	protected $_tag;

	/**
	 * @var array
	 */
	protected $_wrap = array();

	/**
	 * @var array
	 */
	protected $_elements = array();

	/**
	 * Renders Element
	 *
	 * @return string
	 */
	public function renderElement()
	{
		$elementsHtmlString = '';
		foreach ($this->_elements as $element) {
			$elementsHtmlString .= $element->render(array());
			$this->_isValid &= $element->getValidity();
		}

		return $elementsHtmlString;
	}

	/**
	 * @param array $formValuesArray
	 * @return bool
	 */
	public function isValid($formValuesArray = array())
	{
		return true;
	}

	/**
	 * Adds an element object to the array for rendering
	 *
	 * @param AbstractElement $element
	 */
	public function addElement(\Nomad\Form\Element\AbstractElement $element)
	{
		$this->_elements[] = $element;
		$element->rendered = true;
	}

	/**
	 * @return array
	 */
	public function getWrapped()
	{
		return $this->_wrap;
	}

	/**
	 * @return array
	 */
	public function getElements()
	{
		return $this->_elements;
	}
}