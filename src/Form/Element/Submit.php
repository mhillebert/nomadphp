<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Form\Element;
/**
 * Class Submit
 *
 * @package Nomad\Form\Element
 * @author  Mark Hillebert
 */
class Submit
	extends AbstractElement
{
	/**
	 * @var string
	 */
	protected $_originalValue;

	/**
	 * @param string $name
	 * @param array  $params
	 */
	public function __construct($name, $params = array())
	{
		$this->_originalValue = isset($params['value']) ? $params['value'] : $name;
		parent::__construct($name, $params);
	}

	/**
	 * Renders Element
	 *
	 * @return string
	 */
	public function renderElement()
	{
		return "<input type='submit' name='{$this->_name}' {$this->_attributesHtml} {$this->_valueHtml} {$this->_requiredHtml}/>";
	}

	/**
	 * @param array $formValuesArray
	 * @return bool
	 */
	public function isValid($formValuesArray = array())
	{
		if ($this->_originalValue == $this->_value) {
			return true;
		}

		$this->_value           = $this->_originalValue;
		$this->_errorMessages[] = 'Invalid submit button.';

		return false;
	}
}