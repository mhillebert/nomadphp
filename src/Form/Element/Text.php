<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Form\Element;
/**
 * Class Text
 *
 * @package Nomad\Form\Element
 * @author  Mark Hillebert
 */
class Text
	extends AbstractElement
{
	/**
	 * Renders Element
	 *
	 * @return string
	 */
	public function renderElement()
	{
		return "<input type='text' name='{$this->_name}' {$this->_attributesHtml} {$this->_valueHtml} {$this->_requiredHtml}/>";
	}

	/**
	 * @param array $formValuesArray
	 * @return bool
	 */
	public function isValid($formValuesArray = array())
	{
		if ($this->_required && is_null($this->getValue())) {
			return false;
		}

		return true;
	}
}