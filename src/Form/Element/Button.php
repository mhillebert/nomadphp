<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Form\Element;

/**
 * Class Button
 *
 * @package Nomad\Form\Element
 * @author  Mark Hillebert
 */
class Button
	extends AbstractElement
{
	/**
	 * @var null
	 */
	protected $_originalValue;

	/**
	 * @param string $name
	 * @param array  $params
	 */
	public function __construct($name, $params = array())
	{
		$this->_originalValue = isset($params['value']) ? $params['value'] : null;
		parent::__construct($name, $params);
	}

	/**
	 * Renders Element
	 *
	 * @return string
	 */
	public function renderElement()
	{
		$this->_valueHtml = !isset($this->_originalValue) ? "" : "value='{$this->_originalValue}'";

		return "<input type='button' name='{$this->_name}' {$this->_attributesHtml} {$this->_valueHtml} {$this->_requiredHtml}/>";
	}

	/**
	 * @param array $formValuesArray
	 * @return bool
	 */
	public function isValid($formValuesArray = array())
	{
		return true;
	}
}