<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Form\Element;

/**
 * Class Link
 *
 * @package Nomad\Form\Element
 * @author  Mark Hillebert
 */
class Link
	extends AbstractElement
{
	/**
	 * @var string
	 */
	protected $_originalValue;

	/**
	 * @var string
	 */
	protected $_href;

	/**
	 * @param string $name
	 * @param array  $params
	 */
	public function __construct($name, $params = array())
	{
		$this->_originalValue = isset($params['value']) ? $params['value'] : $name;
		parent::__construct($name, $params);
	}

	/**
	 * Renders Element
	 *
	 * @return string
	 */
	public function renderElement()
	{
		return "<a href='{$this->_href}' {$this->_attributesHtml}>{$this->_value}</a>";
	}

	/**
	 * @param array $formValuesArray
	 * @return bool
	 */
	public function isValid($formValuesArray = array())
	{
		return true;
	}
}