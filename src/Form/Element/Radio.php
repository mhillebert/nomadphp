<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Form\Element;

use Nomad\Exception\Form;

/**
 * Class Radio
 *
 * @package Nomad\Form\Element
 * @author  Mark Hillebert
 */
class Radio
	extends AbstractElement
{
	/**
	 * @var array
	 */
	protected $_options;

	/**
	 * @var string
	 */
	protected $_optionTag;

	/**
	 * @var string
	 */
	protected $_optionWrapperTag;
	protected $_optionWrapperTagAttributes;

	/**
	 * @var null
	 */
	protected $_originalValue;

	/**
	 * @var string
	 */
	protected $_elementWrapper = 'div';
	/**
	 * @var string Use this OR optionTag/optionTagWrapper to separate the option;
	 */
	protected $_optionSeparator = '<br/>';

	/**
	 * @param string $name
	 * @param array  $params
	 * @throws Form
	 */
	public function __construct($name, $params = array())
	{
		if (!isset($params['options'])) {
			throw new Form("Radio button must be passed an options array during initialization.");
		}

		$this->_originalValue = isset($params['value']) ? $params['value'] : null;
		parent::__construct($name, $params);
	}

	/**
	 * Renders Element
	 *
	 * @return string
	 */
	public function renderElement()
	{
		$elementHtml = '';
		foreach ($this->_options as $key => $option) {
			$valueHtml      = "value='{$key}'";
			$selectedString = $this->_value != $key ? "" : "checked='checked'";
			$optionHtml     = "<input type='radio' name='{$this->_name}' {$valueHtml} {$this->_requiredHtml} {$this->_attributesHtml} {$selectedString}>{$option}";
			$usingOptionTag = isset($this->_optionWrapperTag);
			if ($usingOptionTag) {
				$wrapperAttributes = isset($this->_optionWrapperTagAttributes) ? $this->_createAttributeString($this->_optionWrapperTagAttributes) : null;
				$optionHtml        = "<{$this->_optionWrapperTag} {$wrapperAttributes}>{$optionHtml}</{$this->_optionWrapperTag}>";
			}
			else {
				$optionHtml .= $this->_optionSeparator;
			}
			$elementHtml .= $optionHtml;
		}
		if ($this->_elementWrapper) {
			$html = "<{$this->_elementWrapper} id='{$this->_name}'>{$elementHtml}</{$this->_elementWrapper}>";
		}
		else {
			$html = rtrim($elementHtml, $this->_optionSeparator); // remove the last separator added
		}

		return $html;
	}

	/**
	 * Internal check for isValid.
	 * Make sure the select values is one of the 'set' values and hasn't been altered.
	 *
	 * @param array $formValues
	 * @return bool
	 */
	public function isValid($formValues = array())
	{
		if ($this->_beenSubmitted) {
			$this->_required = (bool)$this->_required;
			$value           = $this->getValue();
			if ($this->_required && (is_null($value) || empty($value))) {
				return false;
			}

			$selectedValue = $this->getValue();
			if ($this->_required && !\array_key_exists($selectedValue, $this->_options)) {
				$this->_errorMessages[] = "Invalid selection.";
				$this->_value           = $this->_originalValue;

				return false;
			}
		}

		return true;
	}

	/**
	 * Options getter
	 *
	 * @return mixed
	 */
	public function getOptions()
	{
		return $this->_options;
	}
}