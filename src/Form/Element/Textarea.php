<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Form\Element;
/**
 * Class Textarea
 *
 * @package Nomad\Form\Element
 * @author  Mark Hillebert
 */
class Textarea
	extends AbstractElement
{
	/**
	 * Renders Element
	 *
	 * @return string
	 */
	public function renderElement()
	{
		return "<textarea name='{$this->_name}' {$this->_attributesHtml} {$this->_requiredHtml}>{$this->_value}</textarea>";
	}

	/**
	 * @param array $formValuesArray
	 * @return bool
	 */
	public function isValid($formValuesArray = array())
	{
		$value = $this->getValue();
		if ($this->_required && empty($value)) {
			$this->_errorMessages[] = "This is required.";

			return false;
		}

		return true;
	}
}