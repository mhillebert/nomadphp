<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Form\Element;

/**
 * Class Html
 *
 * @package Nomad\Form\Element
 * @author  Mark Hillebert
 */
class Html
	extends AbstractElement
{
	/**
	 * @var bool
	 */
	public $ignore = true;

	/**
	 * Renders Element
	 *
	 * @return string
	 */
	public function renderElement()
	{
		return $this->_value;
	}

	/**
	 * @param array $formValuesArray
	 * @return bool
	 */
	public function isValid($formValuesArray = array())
	{
		return true;
	}
}