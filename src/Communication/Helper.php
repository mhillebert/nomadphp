<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Communication;
/**
 * Class Helper
 *
 * @package Nomad\Communication
 * @author  Mark Hillebert
 */
class Helper
{
	/**
	 * @param $filename
	 * @return string
	 */
	public function base64image($filename)
	{
		$type = substr(strrchr($filename, '.'), 1);
		switch ($type) {
			case 'jpg' :
				$encodeType = 'jpeg';
				break;
			case 'png' :
				$encodeType = 'png';
				break;
			default:
				$encodeType = 'jpeg';
		}

		return "data:image/{$encodeType};base64," . base64_encode(file_get_contents(PUBLIC_ROOT . $filename));
	}
}