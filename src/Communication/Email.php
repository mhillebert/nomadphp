<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Communication;

use Nomad\Core\View;
use Nomad\Exception\Communication;

/**
 * Class Email
 *
 * @package Nomad\Communication
 * @author  Mark Hillebert
 */
class Email
{
	const HTML_EMAIL = 'html';
	const TEXT_EMAIL = 'text';

	/**
	 * @const string
	 */
	const DEFAULT_MIME_VERSION = "MIME-Version: 1.0";
	const DEFAULT_HTML_CONTENT_TYPE = "Content-Type: text/html; charset='iso-8859-1'";
	const DEFAULT_TEXT_CONTENT_TYPE = "Content-Type: text/plain; charset='iso-8859-1'";
	const MULTI_CONTENT_TYPE = "Content-Type: multipart/alternative; boundary=";

	const DEFAULT_TRANSFER_ENCODING = 'Content-Transfer-Encoding: 7bit';

	/**
	 * @var string
	 */
	protected $_type = self::HTML_EMAIL;

	/**
	 * @var array
	 */
	protected $_senders = array();

	/**
	 * @var array
	 */
	protected $_cc = array();

	/**
	 * @var array
	 */
	protected $_bcc = array();

	/**
	 * @var array
	 */
	protected $_recipients = array();

	/**
	 * @var string
	 */
	protected $_subject;

	/**
	 * @var string
	 */
	protected $_body;

	/**
	 * @var \Nomad\Core\View
	 */
	public $view;

	/**
	 * Annotated View Path:
	 * eg: User\\Email\accountVerification     points to
	 * APPLICATION_ROOT\package\User\view\Email\accountVerification.phtml
	 *
	 * @param $AnnotatedViewPath
	 */
	public function __construct($AnnotatedViewPath)
	{
		$this->setView($AnnotatedViewPath);
	}

	/**
	 * @param $sender
	 */
	public function addSender($sender)
	{
		$this->_senders[] = $sender;
	}

	/**
	 * @param $recipient
	 */
	public function addRecipient($recipient)
	{
		$this->_recipients[] = $recipient;
	}

	/**
	 * @param array $recipients
	 */
	public function setBcc(array $recipients)
	{
		$this->_bcc = $recipients;
	}

	/**
	 * @param $subject
	 */
	public function setSubject($subject)
	{
        $this->_subject = preg_replace('/[\r\n]/', '', $subject);
	}

	/**
	 * @param $body
	 */
	public function setBody($body)
	{
		$this->_body = $body;
	}

	/**
	 * sends the email
	 */
	public function send()
	{
		$unique  = "PHP-alt-" . sha1(uniqid());
		$content = $this->_getBody();

		$from    = trim(trim(implode(', ', $this->_senders), ','));
		$to      = trim(trim(implode(', ', $this->_recipients), ','));
		$headers = "From: " . $from . "\r\nReply-To: " . $from . "\r\n";
		$headers .= self::DEFAULT_MIME_VERSION . "\r\n" . self::MULTI_CONTENT_TYPE . $unique . "\r\n";
		if ($this->_bcc) {
			$headers .= "Bcc: " . implode(', ', $this->_bcc) . "\r\n";
		}
		$body = '';

		$body .= "--" . $unique . "\r\n";
		$body .= self::DEFAULT_TEXT_CONTENT_TYPE . "\r\n" . self::DEFAULT_TRANSFER_ENCODING . " \r\n\r\n";
		$body .= strip_tags($content) . "\r\n\r\n";

		$body .= "--" . $unique . "\r\n";
		$body .= self::DEFAULT_HTML_CONTENT_TYPE . "\r\n" . self::DEFAULT_TRANSFER_ENCODING . " \r\n\r\n";
		$body .= $content . "\r\n\r\n";

		$body .= "--" . $unique . "--";

		$emailResult = mail($to, $this->_subject, $body, $headers);

		return $emailResult;
	}

	/**
	 * @param $viewFile string
	 */
	public function setView($viewFile)
	{
		$view         = new View(array('view' => $viewFile));
		$view->helper = new Helper();
		$this->view   = $view;
	}

	/**
	 * @return string
	 * @throws Communication
	 */
	protected function _getBody()
	{
		if (!$this->view) {
			throw new Communication("There is no view associated with this email.");
		}
		$this->view->setIsCachable(false);
		ob_start();
		$this->view->body = $this->_body;
		$this->view->subject = $this->_subject;
		echo $this->view->render();
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}
}