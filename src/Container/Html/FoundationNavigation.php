<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Container\Html;

use Nomad\Core\Session;
use Nomad\Exception as Exception;

/**
 * Class FoundationNavigation
 *
 * @package Nomad\Container\Html
 * @author  Mark Hillebert
 */
class FoundationNavigation
	extends Generic
{
	/**
	 * @param array $options
	 * @throws Exception\Container
	 */
	public function __construct($options = array())
	{
		$options['tag']        = 'nav';
		$options['attributes'] = array(
			'class'       => 'top-bar',
			'role'        => 'navigation',
			'data-topbar' => ''
		);
		parent::__construct($options);
		$linkSection = new Generic(
			array(
				'identifier' => 'links',
				'tag'        => 'section',
				'attributes' => array(
					'class' => 'top-bar-section'
				)
			));
		$titleUl     = new Generic(
			array(
				'identifier' => 'title-area',
				'tag'        => 'ul',
				'attributes' => array(
					'class' => 'title-area',
				)
			));
		$titleHtml   = isset($options['title']) ? "<h1><a href='#'>{$options['title']}</a>" : "";
		$title       = new Generic(
			array(
				'identifier' => 'title',
				'tag'        => 'li',
				'attributes' => array(
					'class' => 'name'
				),
				'text'       => $titleHtml,
			));
		$titleUl->appendChild($title);

		$compactMenuIcon = new Generic(
			array(
				'identifier' => 'menu-toggle',
				'tag'        => 'li',
				'attributes' => array(
					'class' => 'toggle-topbar menu-icon'
				),
				'text'       => "<a href='#'><span>Menu</span></a>"
			));
		$titleUl->appendChild($compactMenuIcon);
		parent::appendChild($titleUl);
		$firstUl = new Generic(
			array(
				'identifier' => 'main-section',
				'tag'        => 'ul',
			));

		$linkSection->appendChild($firstUl);
		parent::appendChild($linkSection);
	}

	/**
	 * Add link to menu
	 *
	 * @param array $options
	 * @throws \Nomad\Exception\Container
	 * @return $this
	 */
	public function addLink($options = array())
	{
		if (!isset($options['label'])) {
			throw new Exception\Container("You must pass in a label in the options array.");
		}

		if (isset(Session::getSession()->aco) && isset($options['role'])) {
			Session::getSession()->aco->addResource($options['label'], $options['role']);
			if (!Session::getSession()->aco->isPermitted($options['label'], $options['role'])) {
				return $this;
			}
		}

		if (!isset($options['href']) || !isset($options['label'])) {
			throw new Exception\Container('You must pass both a href and label');
		}

		$li   = new Generic(array_merge(array('tag' => 'li'), $options));
		$text = $options['label'];
		$href = $options['href'];
		unset($options['href'], $options['label']);
		$li->appendChild(
			new Generic(
				array(
					'tag'        => 'a',
					'attributes' => array(
						'href' => $href,
					),
					'text'       => $text
				)));
		$attributes = isset($options['attributes']) ? $options['attributes'] : array();
		$this->findChild('main-section')->appendChild($li);

		return $this;
	}

	/**
	 * Add sub-menu to existing navigation item
	 *
	 * @param       $identifier
	 * @param array $options
	 * @return $this
	 * @throws \Nomad\Exception\Container
	 */
	public function addSubMenuTo($identifier, $options = array())
	{
		if (!isset($options['menu'])) {
			throw new Exception\Container('addSubMenu expects `menu` as a key.');
		}

		$ul = new Generic(array('tag' => 'ul'));
		if (isset($options['ulClass'])) {
			$ul->addClass($options['ulClass']);
		}

		foreach ($options['menu'] as $item) {
			$li = new Generic(array('tag' => 'li'));

			if (!isset($item['href'])) {//just a label
				$li->appendChild(
					new Generic(
						array(
							'tag'  => 'label',
							'text' => $item['label']
						)));
			}
			else {
				$li->appendChild(
					new Generic(
						array(
							'tag'        => 'a',
							'attributes' => array('href' => $item['href']),
							'text'       => $item['label']
						)));
			}
			$ul->appendChild($li);
		};
		$children = $this->findChildren($identifier);

		if (is_array($children)) {
			foreach ($children as $child) {
				if (isset($options['liClass'])) {
					$child->addClass($options['liClass']);
				}

				$child->appendChild($ul);
			}
		}
		else {
			if (isset($options['liClass'])) {
				$children->addClass($options['liClass']);
			}

			$children->appendChild($ul);
		}

		return $this;
	}
}