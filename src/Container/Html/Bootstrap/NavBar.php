<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Container\Html\Bootstrap;

use Nomad\Container\Html\Generic;

/**
 * Class NavBar
 * This class is messy and can be done much better
 *
 * @package Nomad\Container\Html\Bootstrap
 * @author  Mark Hillebert
 */
class NavBar
	extends \Nomad\Container\Html\BootstrapNavigation
{
	/**
	 * @var bool
	 */
	protected $_collapsible = true;

	/**
	 * @var array
	 */
	protected $_attributes = array(
		'class' => 'navbar navbar-default',
		'role'  => 'navigation'
	);

	/**
	 * @param array $options
	 * @throws \Nomad\Exception\Container
	 */
	public function __construct($options = array())
	{

		if (isset($options['collapse'])) {
			$this->_collapsible = $options['collapse'];
		}

		$options['tag'] = 'div';

		$mainDiv = new Generic(
			array(
				'tag'        => 'div',
				'attributes' => array(
					'class' => 'container-fluid'
				)
			));

		$navHeader = new Generic(
			array(
				'tag'        => 'div',
				'attributes' => array(
					'class' => 'navbar-header'
				)
			));

		if ($this->_collapsible) {
			//collapsible
			$button     = new Generic(
				array(
					'tag'        => 'button',
					'attributes' => array(
						'data-target' => '.navbar-collapse' . (isset($options['name']) ? '.' . $options['name'] : ''),
						'data-toggle' => 'collapse',
						'type'        => 'button',
						'class'       => 'navbar-toggle'
					)
				));
			$buttonLine = new Generic(
				array(
					'tag'        => 'span',
					'attributes' => array(
						'class' => 'icon-bar'
					)
				));
			if (isset($options['title'])) {
				$buttonLink = new Generic(
					array(
						'tag'        => 'a',
						'text'       => isset($options['title']) ? $options['title'] : '',
						'attributes' => array(
							'class' => 'navbar-brand',
							'href'  => '#'
						)
					));
				$button->appendChild($buttonLink);
			}

			$button->appendChild($buttonLine);
			$button->appendChild($buttonLine);
			$button->appendChild($buttonLine);

			$navHeader->appendChild($button);
		}

		$mainDiv->appendChild($navHeader);

		$collapsingClass = $this->_collapsible ? 'navbar-collapse collapse ' : '';
		$mainLinksDiv    = new Generic(
			array(
				'tag'        => 'div',
				'attributes' => array(
					'class' => $collapsingClass . (isset($options['name']) ? $options['name'] : '')
				)
			));
		$firstUl         = new Generic(
			array(
				'identifier' => 'main-section',
				'tag'        => 'ul',
				'attributes' => array(
					'class' => 'nav navbar-nav'
				)
			));
		$mainLinksDiv->appendChild($firstUl);

		$mainDiv->appendChild($mainLinksDiv);
		parent::appendChild($mainDiv);

		parent::__construct($options);
	}

	public function addSubMenuTo($identifier, $options = array())
	{
		parent::addSubMenuTo(
			$identifier, array_merge(
			$options, array(
			'aClass'  => 'dropdown-toggle',
			'liClass' => 'dropdown',
			'ulClass' => 'dropdown-menu'
		)));
	}

	public function disableCollapse()
	{
		$this->_collapsible = false;
	}
}