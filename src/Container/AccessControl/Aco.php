<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Container\AccessControl;

use Nomad\Container\AbstractContainer;
use Nomad\Exception as Exception;

/**
 * Class Aco
 *
 * @package Nomad\Container\AccessControl
 * @author  Mark Hillebert
 */
class Aco
	extends AbstractContainer
{

	/**
	 * Default root node name
	 */
	const DEFAULT_ROOT = 'root';

	/**
	 * @var array|Role
	 */
	protected $_roleTree;

	/**
	 * @var \Nomad\Container\AccessControl\NomadUser
	 */
	protected $_currentUser;

	/**
	 * @var \Nomad\Container\AccessControl\Resource
	 */
	protected $_acl;

	/**
	 * @param string $rootRoleName
	 * @param null   $currentUser
	 * @throws Exception\Aco
	 */
	public function __construct($rootRoleName = 'root', $currentUser = null)
	{
		if (isset($currentUser) && get_class($currentUser) != 'Nomad\Container\AccessControl\User') {
			throw new Exception\Aco('currentUser param must be an instance of Nomad\\Container\\AccessControl\\NomadUser');
		}
		$this->_roleTree = new Role(array('identifier' => $rootRoleName));
	}

	/**
	 * @param       $parentRoleName
	 * @param       $roleName
	 * @param array $asserts
	 * @throws Exception\Container
	 */
	public function addChildRoleTo($parentRoleName, $roleName, $asserts = array())
	{

		$parentRole = $this->_roleTree->findChild($parentRoleName);

		$parentRole->appendChild(
			new Role(array_merge(array('identifier' => $roleName), $asserts))
		);
	}

	/**
	 * @param NomadUser $user
	 */
	public function setCurrentUser(NomadUser $user)
	{
		$this->_currentUser = $user;
	}

	/**
	 * @return NomadUser
	 */
	public function getCurrentUser()
	{
		return $this->_currentUser;
	}

	/**
	 * @param $resource string Name of role aka Identifier
	 * @throws \Nomad\Exception\Aco
	 * @return bool
	 */
	public function isPermitted($resource = null)
	{
		if (empty($resource)) {
			return true;
		}

		if (!isset($this->_acl[$resource])) {
			//maybe this is a route named string... look it up in the routes to get resource
			$route = \Nomad\Core\Registry::get('router')->findRoute($resource);
			if (!$route) {
				throw new Exception\Aco("Resource '{$resource}' not in acl.");
			}
		}

		if (!isset($this->_currentUser)) {

			$this->_currentUser = new NomadUser(['role' => \Nomad\Container\AccessControl\Role::USER_ANONYMOUS]);
		}

		if (in_array(Role::USER_ANONYMOUS, $this->_acl[$resource]->getPermittedRoles()) &&
			$this->_currentUser->getRole() == Role::USER_ANONYMOUS
		) {
			return true;
		}

		if (in_array(Role::USER_VERIFIED, $this->_acl[$resource]->getPermittedRoles()) &&
			$this->_currentUser->getRole() != Role::USER_ANONYMOUS
		) {
			return true;
		}

		return in_array($this->_currentUser->getRole(), $this->getInheritedRoles($this->_acl[$resource]));
	}

	/**
	 * @param                   $resourceName
	 * @param array|string|null $permittedRole
	 * @param array             $asserts
	 * @return $this
	 */
	public function addResource($resourceName, $permittedRole = \Nomad\Container\AccessControl\Role::USER_ANONYMOUS,
								$asserts = array())
	{
		$this->_acl[$resourceName] = new \Nomad\Container\AccessControl\Resource($permittedRole, $asserts);

		return $this;
	}

	/**
	 * Checks if a resource has already been set
	 *
	 * @param $resourceName
	 * @return bool
	 */
	public function isResourceSet($resourceName)
	{
		return isset($this->_acl[$resourceName]);
	}

	/**
	 * Gets inherited roles to a resource
	 *
	 * @param \Nomad\Container\AccessControl\Resource $roleIdentifier
	 * @return array
	 */
	public function getInheritedRoles($roleIdentifier)
	{

		$permittedRoles = $roleIdentifier->getPermittedRoles();
		if (is_array($permittedRoles)) {
			$array = array();
			foreach ($roleIdentifier->getPermittedRoles() as $splitRole) {
				$role = $this->_roleTree->findChild($splitRole);
				if (!empty($role)) {
					$array = array_merge($array, $this->_buildInheritedRoles($role));
				}
			}
			$array = array_unique($array);
		}
		else {
			$roles = $this->_roleTree->findChild($permittedRoles);
			$array = $this->_buildInheritedRoles($roles);
		}

		return $array;
	}

	/**
	 * Gets an array of roles including given and above.
	 *
	 * @param $role
	 * @return array
	 */
	public function getRolesThatInherit($role)
	{
		$roles = $this->_buildInheritedRoles($this->_roleTree->findChild($role));
		array_pop($roles); //remove 'root'
		return $roles;
	}

	/**
	 * Returns array of roles including and below given. aka REAL inherited
	 *
	 * @TODO refactor other name to be better
	 * @param       $role
	 * @param array $array
	 * @return array
	 */
	protected function _getRealInheritedRoles($role, $array = array())
	{
		$array[] = $role->identifier;

		foreach ($role->getChildren() as $child) {
			$array = $this->_getRealInheritedRoles($child, $array);
		}

		return $array;
	}

	/**
	 * @param       $role \Nomad\Container\AccessControl\Role
	 * @param array $array
	 * @return array Array of role identifier and the names of all children role identifiers
	 */
	protected function _buildInheritedRoles($role, $array = array())
	{
		$array[] = $role->identifier;

		while ($role->identifier != $this->_roleTree->identifier) {
			$role    = $role->getParent();
			$array[] = $role->identifier;
		}

		return $array;
	}
}