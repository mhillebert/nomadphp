<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Container\AccessControl;

/**
 * Class Resource
 *
 * @package Nomad\Container\AccessControl
 * @author  Mark Hillebert
 */
class Resource
{
	/**
	 * @var array|\Nomad\Container\AccessControl\Role
	 */
	protected $_permittedRoles;

	/**
	 * @var array
	 */
	protected $_asserts;

	/**
	 * @param string $permittedRole
	 * @param array  $asserts
	 */
	public function __construct($permittedRole = \Nomad\Container\AccessControl\Role::USER_ANONYMOUS,
								$asserts = array())
	{
		$this->_permittedRoles = $permittedRole;
		if ($asserts) {
			foreach ($asserts as $key => $assert) {
				$this->_asserts[$key] = $asserts;
			}
		}
	}

	/**
	 * @return array|Role|string
	 */
	public function getPermittedRoles()
	{
		if (is_array($this->_permittedRoles)) {
			return $this->_permittedRoles;
		}

		return (array)$this->_permittedRoles;
	}
}