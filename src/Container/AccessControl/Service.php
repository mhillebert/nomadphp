<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Container\AccessControl;

use Nomad\Core\Annotations;
use Nomad\Core\Session;
use Nomad\Unify as Unify;
use Nomad\Core\Inspector;
use Nomad\Exception as Exception;

/**
 * Class Service
 *
 * @package Nomad\Container\AccessControl
 * @author  Mark Hillebert
 */
abstract class Service
{
	/**
	 * ACL constants
	 */
	const ACL_CREATE = 'create';
	const ACL_READ = 'read';
	const ACL_UPDATE = 'update';
	const ACL_DELETE = 'delete';

	/**
	 * @var string
	 */
	protected $_aclAction;

	/**
	 * @var array services declared from class annotations
	 */
	protected $_services;

	/**
	 * @var \Nomad\Core\Inspector
	 */
	protected $_inspector;

	/**
	 * constructor
	 */
	public function __construct($options = array())
	{
		if (!isset($options['inspector']) || (isset($options['inspector']) && $options['inspector'] === true)) {
			$this->_inspector = new Inspector($this);
		}
		else {
			$this->_inspector = false;
		}
		if ($this->_inspector) {
			$meta = $this->_inspector->getClassMeta();

			if (isset($meta[Annotations::$service])) {
				foreach ($meta[Annotations::$service] as $service) {
					if (strpos($service, '=>') !== false) { // allow for naming
						$serviceAnnotationBreakdown                                     = explode('=>', $service);
						$this->_services[trim(ucfirst($serviceAnnotationBreakdown[0]))] = trim($serviceAnnotationBreakdown[1]);
					}
					else {
						//Were not instantiating the service until its called
						$serviceParts                              = explode('\\', $service);
						$this->_services[array_pop($serviceParts)] = $service;
					}
				}
			}
		}
	}

	/**
	 * Handles Service calls if call contains _get{someService}Service;
	 *
	 * @param $name
	 * @param $arguments
	 * @throws \Nomad\Exception\Service
	 * @return $this|null
	 */
	public function __call($name, $arguments)
	{
		$getMatches = \Nomad\Unify\StringMethods::match($name, "^_get([a-zA-Z0-9]+)Service$");
		if (count($getMatches) > 0) {
			//add a slash before each capital letter (except first)
			$class = str_replace('\\', '', preg_replace('/(\w+)([A-Z])/U', '\\1\\\\\2', $getMatches[0]));

			if ($this->_services && array_key_exists($class, $this->_services)) {

				if (is_string($this->_services[$class])) {
					$this->_services[$class] = new $this->_services[$class];
				}

				if ($arguments) {
					if (!is_array($arguments[0])) {
						throw new Exception\Service('Injection calls with arguments must be passed in as `method`=>`params` array.');
					}

					foreach ($arguments[0] as $property => $value) {
						$this->_services[$class]->$property($value);
					}
				}

				return $this->_services[$class];
			}
			else {
				//asked for a service that was not annotated
				throw new Exception\Service("`{$class}Service` has not been injected. Annotation missing or parent::__construct() not called in " . get_class($this));
			}
		}
	}

	/**
	 * @param $model
	 * @return bool
	 */
	public function isAllowed($model)
	{
		if (!$model) {
			return false;
		}
		$rules    = $this->_inspector->getClassMeta();
		$checkFor = '@Nomad\\Acl\\' . ucfirst($this->_aclAction);
		if (isset($rules[$checkFor])) {
			$aco           = Session::getSession()->aco;
			$ownerProperty = isset($rules[Annotations::$aclOwnerProperty]) ? array_shift($rules[Annotations::$aclOwnerProperty]) : null;
			$userProperty  = isset($rules[Annotations::$aclUserProperty]) ? array_shift($rules[Annotations::$aclUserProperty]) : null;
			$permission    = false;
			foreach ($rules[$checkFor] as $role) {
				if ($ownerProperty && strtoupper($role) == 'OWNER') {
					$modelProperty = 'get' . $ownerProperty;
					$userProperty  = 'get' . $userProperty;

					if ($aco->getCurrentUser()->$userProperty() == $model->$modelProperty(false)) {
						$permission |= true;
					}
				}
				else {
					if (in_array($aco->getCurrentUser()->getRole(), $aco->getRolesThatInherit($role))) {
						$permission |= true;
					}
					else {
						$permission |= false;
					}
				}
			}

			return (bool)$permission;
		}
	}

	/**
	 * @param $action
	 */
	public function setAclAction($action)
	{
		$this->_aclAction = $action;
	}

	/**
	 * @param array $results
	 * @return bool
	 */
	protected function _isMaxOfOne(array $results)
	{
		$count = count($results);
		if ($count <= 1 && isset($results[0])) {
			return true;
		}
		else {
			return false;
		}
	}
}