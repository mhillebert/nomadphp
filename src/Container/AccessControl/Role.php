<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Container\AccessControl;

/**
 * Class Role
 *
 * @package Nomad\Container\AccessControl
 */
class Role extends \Nomad\Container\Plain
{
    /**
     * @const Only users that are not logged in nor verified.
     */
    const USER_ANONYMOUS = 'anonymous';

    /**
     * @const Any user that has logged in.
     */
    const USER_VERIFIED = 'verified';

    /**
     * Name of role aka Identifier
     * @var string
     */
    protected $_role;

	/**
	 * @var array
	 */
    protected $_asserts;

	/**
	 * @return string
	 */
    public function getRole()
    {
        return $this->_role;
    }
}