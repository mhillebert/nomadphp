<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Container\AccessControl;

use Nomad\Core\Model;

/**
 * Class NomadUser
 *
 * @package Nomad\Container\AccessControl
 * @author  Mark Hillebert
 */
class NomadUser
	extends Model
{
	/**
	 * @var string Role of user.
	 */
	protected $_role;

}