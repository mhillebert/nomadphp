<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Database;

use Nomad\Core as Core;
use Nomad\Exception as Exception;

/**
 * Class Driver
 *
 * @package Nomad\Database
 * @author  Mark Hillebert
 */
class Driver
	extends Core\BaseClass
{
	/**
	 * @return $this
	 */
	public function initialize()
	{
		return $this;
	}
}