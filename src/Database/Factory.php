<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Database;

use Nomad\Core as Core;
use Nomad\Exception as Exception;

/**
 * Class Factory
 * Factory database class
 *
 * @package Nomad\Database
 * @author  Mark Hillebert
 */
class Factory
	extends Core\BaseClass
{
	/**
	 * @var string type of class to create
	 */
	protected $_type;

	/**
	 * @var mixed Options
	 */
	protected $_options;

	/**
	 * @param array $options
	 */
	public function __construct(array $options = array())
	{
		$this->_type = isset ($options['type']) ? $options['type'] : null;
		unset($options['type']);
		$this->_options = $options;
		parent::__construct();
	}

	/**
	 * @return Driver\MySQL
	 * @throws Exception\Argument
	 * @throws Exception\Database
	 */
	public function initialize()
	{
		if (!$this->_type) {
			throw new Exception\Database("Database type cannot be empty");
		}
		switch (strtolower($this->_type)) {
			case "mysql": { //($dbType, $dbName, $dbHost, $dbUsername, $dbPassword = '')
				if (!isset($this->_options['database']) || !isset($this->_options['host'])
					|| !isset($this->_options['username']) || !isset($this->_options['password'])
				) {
					throw new Exception\Database("MySQL Connection options are missing.");
				}
				$mySql = new Driver\MySQL($this->_options);

				return $mySql;

				break;
			}
			default: {
				throw new Exception\Argument("Unknown Database type: {$this->_type}.");
			}
		}
	}
}