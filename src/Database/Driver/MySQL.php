<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Database\Driver;

use Nomad\Exception as Exception;

/**
 * Class MySQL
 *
 * @package Nomad\Database\Driver
 * @author  Mark Hillebert
 */
class MySQL
	extends \PDO
{
	/**
	 * Comparison Operators
	 */
	const EQUAL_TO = '=';
	const NOT_EQUAL_TO = '!=';
	const LESS_THAN = '<';
	const LESS_THAN_OR_EQUAL_TO = '<=';
	const GREATER_THAN = '>';
	const GREATER_THAN_OR_EQUAL_TO = '>=';
	const BETWEEN = "BETWEEN";
	const LIKE = "LIKE";
	const MYSQL_NULL = "IS NULL";
	const NOT_NULL = "IS NOT NULL";

	/**
	 * Logical Operators
	 */
	const OR_WHERE = "OR";
	const AND_WHERE = "AND";

	/**
	 * Query types
	 */
	const INSERT = 'INSERT';
	const SELECT = 'SELECT';
	const DELETE = 'DELETE';
	const UPDATE = 'UPDATE';

	/**
	 * DEFAULTS
	 */
	const DEFAULT_ORDER_BY = "ASC";

	/**
	 * @var array
	 */
	protected $_allowedOperators = array(
		self::EQUAL_TO,
		self::NOT_EQUAL_TO,
		self::LESS_THAN,
		self::LESS_THAN_OR_EQUAL_TO,
		self::GREATER_THAN,
		self::GREATER_THAN_OR_EQUAL_TO,
		self::BETWEEN,
		self::OR_WHERE,
		self::AND_WHERE,
		self::LIKE,
		self::MYSQL_NULL,
		self::NOT_NULL
	);

	protected $_unboundable = array(
		self::MYSQL_NULL,
		self::NOT_NULL
	);

	/**
	 * @var string The Current working table for all future queries.
	 */
	protected $_table;

	/**
	 * @var string The query to perform: INSERT, UPDATE, DELETE, SELECT
	 */
	protected $_queryType;
	/**
	 * @var string The query to execute
	 */
	protected $_queryString;

	/**
	 * @var PDO Statement Object
	 */
	protected $_statement;

	/**
	 * Array of columns to be selected
	 *
	 * @var array
	 */
	protected $_selectColumns;

	/**
	 * @var array Array of Join clauses
	 */
	protected $_joins;

	/**
	 * Array of where clauses
	 * e.g. "AND ID = :id", "OR name = :name"
	 *
	 * @var array
	 */
	protected $_where;

	/**
	 * @var array Array of prepared values for where statement
	 */
	protected $_whereValues = array();

	/**
	 * @var array Raw insert data for query
	 */
	protected $_insertData;

	/**
	 * @var array insert values for prepared insert
	 */
	protected $_insertValues = array();

	/**
	 * @var array Raw update data for query
	 */
	protected $_updateData;

	/**
	 * @var array update values for prepared update
	 */
	protected $_updateValues = array();

	/**
	 * @var array order by columns=>direction
	 */
	protected $_orderByColumns;

	/**
	 * @var string Limit clause (may comma delimited contain offset)
	 */
	protected $_limitClause;

	/**
	 * @var array GROUP BY columns
	 */
	protected $_groupByColumns;

	/**
	 * Instantiate a PDO Instance
	 *
	 * @param array $connectionParams
	 * @throws \Nomad\Exception\Database
	 *
	 * @return \Nomad\Database\Driver\MySQL
	 */
	public function __construct(array $connectionParams)
	{
		try {
			$dbType     = 'mysql';
			$dbName     = $connectionParams['database'];
			$dbHost     = $connectionParams['host'];
			$dbUsername = $connectionParams['username'];
			$dbPassword = $connectionParams['password'];

			$databaseString = sprintf("%s:dbname=%s;host=%s", $dbType, $dbName, $dbHost);
			parent::__construct($databaseString, $dbUsername, $dbPassword);
			$this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
			$this->setAttribute(\PDO::ATTR_EMULATE_PREPARES, true);
			$this->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
			$this->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
		}
		catch (\PDOException $e) {
			throw new Exception\Database("Unable to connect to the database.");
		}

		return $this;
	}

	/**
	 * Sets the table as the working table for future queries
	 * ALso, clears any previous query
	 *
	 * @var string Table
	 * @return $this
	 */
	public function setWorkingTable($table, $clearAll = true)
	{
		if ($clearAll) {
			$this->clear();
		}
		$this->_table = $table;

		return $this;
	}

	/**
	 * Gets the current working table
	 *
	 * @return string
	 */
	public function getWorkingTable()
	{
		return $this->_table;
	}

	/**
	 * Perform a direct query by passing a full SQL query string.
	 *
	 * @param $queryString
	 * @return int
	 */
	public function directQuery($queryString)
	{
		$command = preg_replace("/\s.*$/", "", $queryString);
		switch (strtoupper($command)) {
			case "SELECT":
				$resultSet = $this->query($queryString);

				return $resultSet->fetchAll();
				break;
			case "INSERT":
				$this->exec($queryString);

				return $this->lastInsertId();
				break;

			case "UPDATE":
			case "DELETE":
			default:
				return $this->exec($queryString);
		}
	}

	/**
	 * Sets up SELECT query.
	 * If no table specified, then the selected table is used.
	 *  Usage:
	 *       ->select("id", "users.value", "roles.role")
	 *       ->leftJoin('roles', 'id', 'users.roleId')
	 *       ->limit(3, 2);
	 * says:
	 *  SELECT users.id, users.value, roles.role FROM users
	 *      LEFT JOIN roles ON roles.id = users.roleId LIMIT 3, 2
	 *
	 * Finally: if the last argument in the argument list is the value TRUE
	 * Then you may use complex selections such as:
	 *      COUNT(*), CONCAT(fname, lname) AS name
	 *
	 * @throws \Nomad\Exception\Database
	 * @return $this
	 */
	public function select()
	{
		$this->_queryType = self::SELECT;
		$columnsArray     = func_get_args();
		if (empty($columnsArray)) {
			throw new Exception\Database('Cannot select zero columns.');
		}

		$argCount = count($columnsArray);
		$complex  = false;
		if (is_bool($columnsArray[$argCount - 1])) {
			$complex = $columnsArray[$argCount - 1];
			unset($columnsArray[$argCount - 1]);
		}

		foreach ($columnsArray as $column) {
			$columnName = $column;
			if (!$complex && strpos($column, '.') === false) {
				if ($this->_table) {
					$columnName = "{$this->_table}.{$column}";
				}
				else {
					$columnName = $column;
				}
			}
			$this->_selectColumns[] = $columnName;
		}

		return $this;
	}

	/**
	 * sets up INSERT query.
	 * Pass in an array of column=>value pairs
	 * Usage:
	 *   ->insert(array(
	 *       'username'=>'Freddy',
	 *       'value' => 11,
	 *       'roleId' => 2));
	 * says: INSERT INTO table ('table.username', 'table.value', 'table.roleId')
	 *              VALUES ('Freddy', 11, 2)
	 *
	 * @return $this
	 */
	public function insert()
	{
		$valuesArr        = func_get_args();
		$this->_queryType = self::INSERT;
		$this->_checkAndSetData($valuesArr, "_insertData", "You must pass an array when inserting.");
	}

	/**
	 * Updates columns on a table.
	 * Usage:
	 *      ->update(array(
	 *              'value' => 0,
	 *              'roleId' => 4,
	 *               ))
	 *      ->andWhere('users.username', '=', 'Cindy')
	 * says:  UPDATE table SET 'users.value' = 0, users.roleId = 4
	 *              WHERE users.username = 'Cindy'
	 *
	 * @return $this
	 */
	public function update()
	{
		$valuesArr        = func_get_args();
		$this->_queryType = self::UPDATE;
		$this->_checkAndSetData($valuesArr, "_updateData", 'You must pass an array when updating.');

		return $this;
	}

	/**
	 * Sets the query type to delete.
	 * This needs to be used with a where clause to specify which rows to delete.
	 * If not, the query will look like this (and delete all)
	 *      DELETE table FROM table
	 *
	 * @return $this
	 */
	public function delete()
	{
		$this->_queryType = self::DELETE;

		return $this;
	}

	/**
	 * Adds AND clause to WHERE clause
	 * If there is only one where then orWhere/andWhere do the same thing otherwise multiple statements are joined
	 * together by the keyword
	 * Usage:
	 *      ->andWhere('price', '<', '50')
	 *      ->andWhere('color', '=', 'red')
	 * says: WHERE table.price < '50' AND table.color = 'red'
	 *
	 * @param string       $column
	 * @param string       $operator
	 * @param string|array $value
	 * @return $this
	 */
	public function andWhere($column, $operator, $value)
	{
		if (is_array($value)) {
			foreach ($value as $singleValue) {
				$this->_appendWhere(self::AND_WHERE, $column, $operator, $singleValue);
			}

			return $this;
		}
		$this->_appendWhere(self::AND_WHERE, $column, $operator, $value);

		return $this;
	}

	/**
	 * @param $column
	 * @throws Exception\Database
	 */
	public function andWhereIsNull($column)
	{
		$this->_appendWhere(self::AND_WHERE, $column, self::MYSQL_NULL, null);
	}

	/**
	 * @param $column
	 * @throws Exception\Database
	 */
	public function andWhereIsNotNull($column)
	{
		$this->_appendWhere(self::AND_WHERE, $column, self::NOT_NULL, null);
	}

	/**
	 * @param $mysqlWhereStatement
	 * @return $this
	 */
	public function injectIntoWhere($mysqlWhereStatement)
	{
		$this->_where[] = $mysqlWhereStatement;

		return $this;
	}

	/**
	 * Adds OR clause to WHERE clause
	 * If there is only one where then orWhere/andWhere do the same thing otherwise multiple statements are joined
	 * together by the keyword
	 * Usage:
	 *      ->orWhere('price', '<', '50')
	 *      ->orWhere('color', '=', 'red')
	 * says: WHERE table.price < '50' OR table.color = 'red'
	 *
	 * @param $column
	 * @param $operator
	 * @param $value
	 * @return $this
	 */
	public function orWhere($column, $operator, $value)
	{
		if (is_array($value)) {
			foreach ($value as $singleValue) {
				$this->_appendWhere(self::OR_WHERE, $column, $operator, $singleValue);
			}

			return $this;
		}
		$this->_appendWhere(self::OR_WHERE, $column, $operator, $value);

		return $this;
	}

	/**
	 * Adds a LEFT JOIN to the query.
	 * Usage:
	 *      ->leftJoin('tableQQ', 'tableQQ column to join', 'selectedTable.column')
	 * ex:
	 *      ->leftJoin('roles', 'id', 'users.id') =
	 *          LEFT JOIN roles ON roles.id=users.id
	 *
	 * @param $table
	 * @param $column
	 * @param $tableDotColumn
	 * @return $this
	 */
	public function leftJoin($table, $column, $tableDotColumn)
	{
		$this->_joins[] = "LEFT JOIN {$table} ON {$table}.{$column} = {$tableDotColumn}";

		return $this;
	}

	/**
	 * Adds a RIGHT JOIN to the query.
	 * Usage:
	 *      ->rightJoin('tableQQ', 'tableQQ column to join', 'selectedTable.column')
	 * ex:
	 *      ->rightJoin('roles', 'id', 'users.id') =
	 *          RIGHT JOIN roles ON roles.id=users.id
	 *
	 * @param $table
	 * @param $column
	 * @param $tableDotColumn
	 * @return $this
	 */
	public function rightJoin($table, $column, $tableDotColumn)
	{
		$this->_joins[] = "RIGHT JOIN {$table} ON {$table}.{$column} = {$tableDotColumn}";

		return $this;
	}

	/**
	 * Adds ORDER BY TO query
	 * Usages:
	 * Single Param - defaults to self::DEFAULT_ORDER_BY (ASC)
	 *    ->orderBy('id')
	 * Two Params - specifies direction.
	 *    ->orderBy('id', 'DESC')
	 *
	 * or Array of multiple columns
	 *    ->orderBy(array(
	 *                  'id' => 'DESC', //note direction specified
	 *                  'name', // note direction not specified (defaults to self::DEFAULT_ORDER_BY)
	 *                  'other_table.date_added',
	 *
	 * @throws \Nomad\Exception\Database
	 * @return $this
	 */
	public function orderBy()
	{
		$valuesArr = func_get_args();
		$argCount  = count($valuesArr);
		$isArray   = is_array($valuesArr[0]);
		if ($argCount > 2 && !$isArray) {
			throw New Exception\Database("Please use an array notation to pass multiple ORDER BY columns.");
		}
		if ($argCount == 2 && isset($valuesArr[1]) && (!is_string($valuesArr[0]) || !is_string($valuesArr[1]))) {
			throw New Exception\Database("ORDER BY arguments not understood.");
		}
		if ($isArray) {
			foreach ($valuesArr[0] as $column => $direction) {
				if (is_int($column)) {
					if (strpos($direction, '.') === false) {
						$direction = $this->_table . '.' . $direction;
					}
					$this->_orderByColumns[$direction] = self::DEFAULT_ORDER_BY;
				}
				else {
					if (strpos($column, 'FIELD') === false && strpos($column, '.') === false) {
						$column = $this->_table . '.' . $column;
					}
					$this->_orderByColumns[$column] = $direction;
				}
			}
		}
		else {
			if (in_array($valuesArr[0], ['RAND()',])) { //a SQL Funciton was passed
				$this->_orderByColumns[$valuesArr[0]] = "";

				return $this;
			}
			else {
				if (strpos($valuesArr[0], '.') === false) {
					$valuesArr[0] = $this->_table . '.' . $valuesArr[0];
				}
			}

			$this->_orderByColumns[$valuesArr[0]] = isset($valuesArr[1]) ? $valuesArr[1] : self::DEFAULT_ORDER_BY;
		}

		return $this;
	}

	/**
	 * Adds a LIMIT to the query
	 *
	 * @param      $limit
	 * @param null $offset
	 * @return $this
	 */
	public function limit($limit, $offset = null)
	{
		$this->_limitClause = "LIMIT " . ($offset ? $offset . ', ' : "") . $limit;

		return $this;
	}

	/**
	 * Adds a GROUP BY clause
	 *
	 * @param $columnName
	 * @return $this
	 */
	public function groupBy($columnName)
	{
		$this->_groupByColumns[] = $columnName;

		return $this;
	}

	/**
	 * echo's the query sent
	 */
	public function devechoQuery()
	{
		$query = $this->_buildQuery();

		foreach ($this->_whereValues as $key => $value) {
			$query = str_replace($key, $value, $query);
		}
		foreach ($this->_updateValues as $key => $value) {
			$query = str_replace($key, $value, $query);
		}
		echo $query;
	}
	/*    @TODO
	public function andIn($column, $array??)
	 * {
	 * }
	 * @TODO
	 * public function existsWhere($column, $array??)
	 * {
	 * }*/

	/**
	 * Executes the query
	 *
	 * @param null $className
	 * @param null $mapper
	 * @throws \Nomad\Exception\Database
	 * @return bool|mixed|string
	 */
	public function executeQuery($className = null, $mapper = null)
	{
		$this->_queryString = $this->_buildQuery();
		$this->_statement   = $this->prepare($this->_queryString);

		switch ($this->_queryType) {
			case self::SELECT:
				if (isset($className)) {
					$this->_statement->execute($this->_whereValues);
					// must manually populate given model because its a Nomad_Model

					$obj = new $className();

					if (is_subclass_of($obj, 'Nomad\\Core\\Model')) {

						try {
							$modelArray = $this->_statement->fetchAll();
						}
						catch (\PDOException $e) {
							throw new Exception\Database("There was an issue with the query.");
						}

						$objArray = array();
						if (isset($mapper)) { //use the supplied mapper
							$modelMapper = new $mapper();
							foreach ($modelArray as $row) {
								$objArray[] = $modelMapper->mapFromDataSource($row);
							}
						}
						else { //no mapper, try to set directly on model
							foreach ($modelArray as $row) {
								$obj = new $className();
								foreach ($row as $property => $value) {
									$function = 'set' . $property;
									$obj->$function($value);
								}
								$objArray[] = $obj;
							}
						}

						return $objArray;
					}
					else {
						//let pdo populate model
						try {
							$modelArray = $this->_statement->fetchAll(\PDO::FETCH_CLASS, $className);
						}
						catch (\PDOException $e) {
							throw new Exception\Database("There was an issue with the query.");
						}

						$this->_where = array();
						unset($this->_where);

						return $modelArray;
					}
				}
				else {
					$results      = $this->_tryExecute($this->_whereValues, 'fetchAll');
					$this->_where = array();
					unset($this->_where);

					return $results;
				}

				break;
			case self::INSERT:
				return $this->_tryExecute($this->_insertValues, 'lastInsertId');
				break;
			case self::UPDATE:
				return $this->_tryExecute(array_merge($this->_updateValues, $this->_whereValues), 'rowCount');
				break;
			case self::DELETE:
				return $this->_tryExecute($this->_whereValues, 'rowCount');
				break;
		}

		return false;
	}

	/**
	 * format: ['key1' => array(val1, val2, val3 ...), key2=> array(valA, valB, valC) ...]
	 * where the indexed values are related.
	 * INSERT ... (key1, key2) VALUES (val1, valA), (val2, valB), (val3, valC) ...
	 *
	 * @param $keyValueArray
	 */
	public function multiRowInsert($keyValueArray)
	{

		$keys         = array_keys($keyValueArray);
		$dataRowCount = count($keyValueArray[$keys[0]]);
		$pdoKeys      = array();
		foreach ($keys as $index => $key) {
			if (strpos($key, '.') === false) {
				$pdoKeys[$index] = $this->_table . '.' . $key;
			}
		}
		$pdoKeyString = "(" . implode($pdoKeys, ', ') . ')';

		$pdoValues          = array();
		$pdoParameterString = '';
		for ($x = 0; $x < $dataRowCount; $x++) {
			$pdoParameters = array();
			foreach ($keys as $key) {
				$pdoValues[]     = array_shift($keyValueArray[$key]);
				$pdoParameters[] = '?';
			}
			$pdoParameterString[] = "(" . implode($pdoParameters, ', ') . ")";
		}
		$pdoParameterString = implode($pdoParameterString, ', ');

		$statement = $this->prepare("INSERT INTO {$this->getWorkingTable()} {$pdoKeyString} VALUES {$pdoParameterString}");
		$statement->execute($pdoValues);
	}

	/**
	 * Wrap a try catch around the execution of the query.
	 * This mainly prevents code duplication.
	 *
	 * @param $preparedValues
	 * @param $returnFunction
	 * @throws \Nomad\Exception\Database
	 * @return bool|mixed|string
	 */
	protected function _tryExecute($preparedValues, $returnFunction)
	{
		try {
			$result = $this->_statement->execute($preparedValues);
		}
		catch (\PDOException $e) {
			throw new Exception\Database($e->getMessage()); //UNComment for real error message otherwise best to keep it obscure.
			//throw new Nomad_Database("There was an error executing the query.");
		}
		if (!$result) {
			return false;
		}
		if ($returnFunction == 'lastInsertId') {
			return $this->lastInsertId();
		}

		return call_user_func(array($this->_statement, $returnFunction));
	}

	/**
	 * Used internally to check the arguments for the public queries or throw an exception.
	 *
	 * @param $args
	 * @param $toVariable
	 * @param $failMessage
	 * @throws \Nomad\Exception\Database
	 * @return $this
	 */
	protected function _checkAndSetData($args, $toVariable, $failMessage)
	{
		if (count($args) == 1 && is_array($args[0])) { //an array was passed in
			$this->{$toVariable} = $args[0];

			return $this;
		}
		else {
			throw new Exception\Database($failMessage);
		}
	}

	/**
	 * Builds a query based on the type (SELECT, INSERT, DELETE, UPDATE)
	 *
	 * @return string
	 */
	protected function _buildQuery()
	{
		$query = "";
		switch ($this->_queryType) {
			case self::INSERT:
				$insertClause = $this->_buildInsertClause();
				$query        = sprintf("INSERT INTO %s %s", $this->_table, $insertClause);
				break;
			case self::SELECT:
				foreach ($this->_selectColumns as &$column) {
					//make sure each selected column is from a table. If there is no dot then add the table
					if (strpos($column, '.') === false) {
						$column = "{$this->_table}.{$column}";
					}
				}
				$selectClause = empty($this->_selectColumns) ? "" : "SELECT " . implode(', ', $this->_selectColumns);
				$joinClause   = empty($this->_joins) ? "" : implode(' ', $this->_joins);
				$whereClause  = empty($this->_where) ? "" : 'WHERE ' . preg_replace('~[^\s]+~', "", implode(' ', $this->_where), 1);
				$orderBy      = empty($this->_orderByColumns) ? "" : "ORDER BY " . $this->_buildOrderByClause();
				$groupBy      = empty($this->_groupByColumns) ? "" : "GROUP BY " . $this->_buildGroupByClause();
				$limit        = empty($this->_limitClause) ? "" : $this->_limitClause;
				$query        = sprintf("%s FROM %s %s %s %s %s %s", $selectClause, $this->_table, $joinClause, $whereClause, $groupBy, $orderBy, $limit);
				break;
			case self::UPDATE:
				$joinClause  = empty($this->_joins) ? "" : implode(' ', $this->_joins);
				$setClause   = $this->_buildSetClause();
				$whereClause = empty($this->_where) ? "" : 'WHERE ' . preg_replace('~[^\s]+~', "", implode(' ', $this->_where), 1);
				$query       = sprintf("UPDATE %s %s %s %s", $this->_table, $joinClause, $setClause, $whereClause);
				break;
			case self::DELETE:
				$joinClause  = empty($this->_joins) ? "" : implode(' ', $this->_joins);
				$whereClause = empty($this->_where) ? "" : 'WHERE ' . preg_replace('~[^\s]+~', "", implode(' ', $this->_where), 1);
				$query       = sprintf("DELETE %s FROM %s %s %s", $this->_table, $this->_table, $joinClause, $whereClause);
				break;
		}

		return $query;
	}

	/**
	 * @return string
	 */
	protected function _buildGroupByClause()
	{
		$clause = "";
		foreach ($this->_groupByColumns as $column) {
			$clause .= "{$column}, ";
		}

		return rtrim($clause, ', ');
	}

	/**
	 * Builds the ORDER BY clause
	 *
	 * @return string
	 */
	protected function _buildOrderByClause()
	{
		$clause = "";
		foreach ($this->_orderByColumns as $column => $direction) {
			$clause .= "{$column} {$direction}, ";
		}

		return rtrim($clause, ', ');
	}

	/**
	 * Builds the SET clause for UPDATES
	 *
	 * @return string
	 */
	protected function _buildSetClause()
	{
		$setString = "";
		foreach ($this->_updateData as $key => $value) {
			if (strpos($key, '.') === false) {
				$key = $this->_table . '.' . $key;
			}
			$transform = "u_" . str_replace('.', '_', $key);
			$setString .= "{$key}=:{$transform}, ";
			$this->_updateValues[$transform] = $value;
		}
		$setString = rtrim($setString, ', ');

		return "SET " . $setString;
	}

	/**
	 * Builds INSERT clause
	 *
	 * @return string
	 */
	protected function _buildInsertClause()
	{
		$boundNameArray = array();
		foreach ($this->_insertData as $key => $value) {
			if (strpos($key, '.') === false) {
				$key = $this->_table . '.' . $key;
			}
			$transform                       = 'i_' . str_replace('.', '_', $key);
			$boundNameArray[]                = ":" . $transform;
			$this->_insertValues[$transform] = $value;
		}

		return "(" . implode(', ', array_keys($this->_insertData)) . ') VALUES (' . implode(', ', $boundNameArray) . ')';
	}

	/**
	 * Adds a WHERE parameter to the array for later use when building WHERE clause in the query
	 * This mainly prevents code duplication for ->andWhere ->orWhere methods
	 *
	 * @param $type
	 * @param $column
	 * @param $operator
	 * @param $value
	 * @throws \Nomad\Exception\Database
	 */
	protected function _appendWhere($type, $column, $operator, $value)
	{
		if (!in_array($operator, $this->_allowedOperators)) {
			throw new Exception\Database("Operator Not understood in your query.");
		}

		$boundName  = '';
		$columnName = $column;
		if (strpos($column, '.') === false) {
			$columnName = "{$this->_table}.{$column}";
		}
		if (!in_array($operator, $this->_unboundable)) {
			$boundName                      = ':w_' . str_replace('.', '_', $column);
			$this->_whereValues[$boundName] = $value;
		}
		$statement      = "{$type} {$columnName} {$operator} {$boundName}";
		$this->_where[] = $statement;
	}

	/**
	 * Replaces all where values with the new values so another execute can be called.
	 *
	 * @throws \Nomad\Exception\Database
	 * @return $this
	 */
	public function replaceWhereValues()
	{
		$newValues = func_get_args();
		if (count($newValues) != count($this->_whereValues)) {
			throw new Exception\Database('ReplaceWhereValues expects identical number of arguments previously used.');
		}
		$i = 0;
		foreach ($this->_whereValues as $key => $value) {
			$this->_whereValues[$key] = $newValues[$i];
			$i++;
		}

		return $this;
	}

	/**
	 * Clears all query clauses except the table selected
	 */
	public function clear()
	{
		$this->_whereValues  = array();
		$this->_insertValues = array();
		$this->_updateValues = array();
		unset($this->_joins, $this->_selectColumns, $this->_insertData,
			$this->_where, $this->_limitClause, $this->_updateData, $this->_orderByColumns, $this->_groupByColumns);
	}
}
