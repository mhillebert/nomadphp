<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;

use Nomad\Exception as Exception;

/**
 * Class Autoloader
 * SplClassLoader implementation that implements the technical interoperability
 * standards for PHP 5.3 namespaces and class names.
 * based on https://gist.github.com/jwage/221634
 * and http://www.youtube.com/watch?v=1tW9fpfJL1c (parts 2 & 3)
 * @todo    This class works but is pretty crappy
 *
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
class Autoloader
{
	/**
	 * @var string
	 */
	private static $_fileExtension = '.php';

	/**
	 * @var string
	 */
	private static $_namespace;

	/**
	 * @var array
	 */
	private static $_includePath = array();

	/*
	 * @var string
	 */
	private static $_namespaceSeparator = '\\';

	/**
	 * @var array ClassName => filePath
	 */
	private static $_classMap = array();

	/**
	 * Holds custom namespaces to check when no namespace is given
	 *
	 * @var array
	 */
	private static $_registeredNamespaceAliases = array();

	/**
	 * @var array Namespace => callback to execute to change rename the fileName from the className
	 */
	private static $_classNameToFileNameMap = array();

	/**
	 * Include multiple classes in class mapping for irregular and aliases classes
	 * uses addClass;
	 *
	 * @param $classes
	 * @return bool
	 */
	public static function addClasses($classes)
	{
		if (is_array($classes)) {
			foreach ($classes as $namespace => $path) {
				self::addClass($namespace, $path);
			}

			return true;
		}

		return false;
	}

	/**
	 * Adds a class to the classMap
	 *
	 * @param $namespace
	 * @param $path
	 * @return bool
	 */
	public static function addClass($namespace, $path)
	{
		if ($namespace && $path) {
			if (!isset(self::$_classMap[$namespace]) && file_exists($path)) {
				self::$_classMap[$namespace] = $path;

				return true;
			}
		}

		return false;
	}

	/**
	 * Changes all file names for classes within a given namespace for proper autoloading.
	 * For example:
	 *      All classes in the Nomad\Exception namespace are suffixed with _Exception, but
	 *      the filename is without this suffix but still within the Nomad/Exception folder structure.
	 *      So, the class name of Autoloader_Exception is resides in the \Nomad\Exception namespace
	 *      and is in the expected /Vendor/Nomad/Exception folder, but
	 *      the filename is simply Autoloader.php
	 *      calling this function can fix this:
	 *         \Nomad\Core\Autoloader::classNameToFileNameReplace('Nomad\Exception',
	 *              function($className)
	 *              {
	 *                  return return str_replace("_Exception", "", $className);
	 *              });
	 *      'Nomad\Exception' is the namespace
	 *      function($className){...} is the function to execute and needs to return the correct filename to load.
	 *
	 * @param $namespace string The Namespace to which the file naming convention changes.
	 * @param $function  callback The function to execute to change the Class Name to match the file name.
	 * @throws \Exception
	 */
	public static function classNameToFileNameReplace($namespace, $function)
	{
		if (!is_callable($function)) {
			throw new \Exception('A callable function must be passes to classNameToFileNameReplace');
		}
		self::$_classNameToFileNameMap[$namespace] = $function;
	}

	/**
	 * @param $namespace
	 */
	public static function registerNamespaceAlias($namespace)
	{
		if (!in_array($namespace, self::$_registeredNamespaceAliases)) {
			self::$_registeredNamespaceAliases[] = $namespace;
		}
	}

	/**
	 * Sets the namespace separator used by classes in the namespace of this class loader.
	 *
	 * @param string $sep The separator to use.
	 */
	public static function setNamespaceSeparator($sep)
	{
		self::$_namespaceSeparator = $sep;
	}

	/**
	 * Gets the namespace seperator used by classes in the namespace of this class loader.
	 *
	 * @return void|string
	 */
	public static function getNamespaceSeparator()
	{
		return self::$_namespaceSeparator;
	}

	/**
	 * Sets the base include path for all class files in the namespace of this class loader.
	 *
	 * @param string $includePath
	 */
	public static function setIncludePath($includePath)
	{
		if (!in_array($includePath, self::$_includePath)) {
			self::$_includePath[] = $includePath;
		}
	}

	/**
	 * Sets the base include paths for all class files in the namespace of this class loader.
	 *
	 * @throws \Nomad\Exception\Autoloader
	 * Param may an indexed array of paths or a comma separated list of strings
	 */
	public static function setIncludePaths()
	{
		$paths = func_get_args();
		if (!$paths) {
			throw new Exception\Autoloader('You must pass at lease one value to the Autoloader.');
		}
		if (is_array($paths)) {
			foreach ($paths as $includePath) {
				if (is_string($includePath)) {
					self::setIncludePath($includePath);
				}
				else {
					throw new Exception\Autoloader('You must pass strings to the Autoloader.');
				}
			}
		}
	}

	/**
	 * Returns the includePath property
	 *
	 * @TODO Dont think I need this
	 * @return array
	 */
	public static function getIncludes()
	{
		return self::$_includePath;
	}

	/**
	 * Removes a path from included paths in loader
	 *
	 * @param $includePath
	 */
	public static function removeIncludePath($includePath)
	{
		foreach (self::$_includePath as $key => $path) {
			if ($includePath == $path) {
				unset(self::$_includePath[$key]);
				self::$_includePath = array_values(self::$_includePath);
				break;
			}
		}
	}

	/**
	 * Gets the base include path for all class files in the namespace of this class loader.
	 *
	 * @return string $includePath
	 */
	public static function getIncludePath()
	{
		return self::$_includePath;
	}

	/**
	 * Sets the file extension of class files in the namespace of this class loader.
	 *
	 * @param string $fileExtension
	 */
	public static function setFileExtension($fileExtension)
	{
		self::$_fileExtension = $fileExtension;
	}

	/**
	 * Gets the file extension of class files in the namespace of this class loader.
	 *
	 * @return string $fileExtension
	 */
	public static function getFileExtension()
	{
		return self::$_fileExtension;
	}

	/**
	 * Installs this class loader on the SPL autoload stack.
	 */
	public static function register()
	{
		spl_autoload_register('Nomad\Core\Autoloader::loadPSR0Class');
		spl_autoload_register('Nomad\Core\Autoloader::loadRemappedByCallbackClass');
	}

	/**
	 * Uninstalls this class loader from the SPL autoloader stack.
	 */
	public static function unregister()
	{
		spl_autoload_unregister('Nomad\Core\Autoloader::loadPSR0Class');
		spl_autoload_unregister('Nomad\Core\Autoloader::loadRemappedByCallbackClass');
	}

	/**
	 * @todo complete or delete
	 * @param $className
	 */
	public static function loadRemappedByCallbackClass($className)
	{
	}

	/**
	 * Loads the given class or interface.
	 *
	 * @param string $className The name of the class to load.
	 * @return boolean
	 */
	public static function loadPSR0Class($className)
	{
		$called[] = $className;
		if (null === self::$_namespace ||
			self::$_namespace . self::$_namespaceSeparator === substr($className, 0, strlen(self::$_namespace . self::$_namespaceSeparator))
		) {
			$fileName  = '';
			$namespace = '';

			$parts = explode('\\', $className);
			if (count($parts) > 2) {
				$namespacePart = $parts[0] . '\\' . $parts[1];
				$classNamePart = $parts[2];
				//check customized namespace file mapper
				if (isset(self::$_classNameToFileNameMap[$namespacePart])) {
					$classNameToFileNameMap = self::$_classNameToFileNameMap;
					$remappedClassName      = $classNameToFileNameMap[$namespacePart]($classNamePart);
					$className              = $namespacePart . self::$_namespaceSeparator . $remappedClassName;
				}
			}

			$namespaceParts = explode('\\', $className);
			if (count($namespaceParts) > 1) {
				array_splice($namespaceParts, 1, 0, 'src');
			}

			$className = implode('\\', $namespaceParts);
			if (false !== ($lastNamespacePosition = strripos($className, self::$_namespaceSeparator))) {
				$namespace = substr($className, 0, $lastNamespacePosition);
				$className = substr($className, $lastNamespacePosition + 1);
				$fileName  = str_replace(self::$_namespaceSeparator, DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
			}

			//check for the psr0 structure
			$psr0FileName = $fileName . str_replace('_', DIRECTORY_SEPARATOR, $className) . self::$_fileExtension;
			$fileName     = $fileName . $className . self::$_fileExtension;
			foreach (self::$_includePath as $includePath) {
				$psr0FullPath = $includePath . DIRECTORY_SEPARATOR . $psr0FileName;
				$fullPath     = $includePath . DIRECTORY_SEPARATOR . $fileName;
				if (file_exists($psr0FullPath)) {
					require($psr0FullPath);
					return true;
				}
				elseif (file_exists($fullPath)) {
					require($fullPath);
					return true;
				}
				elseif (file_exists(APPLICATION_ROOT . '/../' . $fileName)) {
					require(APPLICATION_ROOT . '/../' . $fileName);
					return true;
				}
			}
			
		}

		return false;
	}
}