<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;

use Nomad\Core as Core;
use Nomad\Exception as Exception;
use Nomad\Router\Route;
use Nomad\Unify\StringMethods;
use Nomad\Container as Container;

/**
 * Class Dispatcher
 *
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
class Dispatcher
	extends BaseClass
{
	/**
	 * The view used when redirecting or running annotation from same class with its own view
	 *
	 * @var View a list of views to send to the renderer
	 */
	protected $_masterView;

	/**
	 * @var View
	 */
	protected $_oldView;

	/**
	 * @var bool Flag to indicate the dispatcher should stop
	 */
	protected $_stop = false;

	/**
	 * @var bool Flag to indicate the dispatcher should stop
	 */
	protected $_continueToRenderer = true;

	/**
	 * @var Route Route to evaluate for dispatch
	 */
	protected $_route;

	/**
	 * @var object Main dispatched object
	 */
	protected $_instance;

	/**
	 * @var Inspector Inspector class for the main instance
	 */
	protected $_instanceInspector;

	/**
	 * @var \Nomad\Container\AccessControl\Aco The Access Control Object
	 */
//    protected $_aco;

	/**
	 * @param array $options
	 */
	public function __construct($options = array())
	{
		$options['inspector'] = false;
		parent::__construct($options);
	}

	/**
	 * Calls a given route, reads and executes appropriate annotations, and main method.
	 *
	 * @param null $route
	 * @return null
	 * @throws \Nomad\Exception\Controller
	 * @throws \Nomad\Exception\Action
	 * @throws \Exception
	 */
	public function dispatch($route = null)
	{
		if ($route) {
			$this->_route = $route;
		}
		if (!$this->route) {
			throw new \Exception('A route must be passed to the dispatcher!');
		}
		//ensure controller name is ucFirst;
		$namespace = $this->_route->namespace;

		$action     = $this->_route->action;
		$strictMode = isset(explode('--', $action)[1]) ? true : false;
		if ($strictMode) {
			$action = str_replace('--strict', '', $action);
		}
		$params = $this->_route->params;
		if (!$params) {
			$params = array();
		}
		if (!class_exists($namespace)) {
			throw new Exception\Controller("Class: '{$namespace}' not found!");
		}

		$this->_instance = new $namespace(
			[
				"route"      => $this->_route,
				"parameters" => $params,
			]);

		if (method_exists($this->_instance, 'getView') && !$this->_masterView) {
			$this->_masterView = $this->_instance->getView();
		}
		if ($this->_oldView) {
			$this->_instance->view->setData(array_merge($this->_masterView->getData(), $this->_oldView->getData()));
			unset($this->_oldView);
		}

		if (!method_exists($this->_instance, $action)) {
			throw new Exception\Action("Method: '{$action}' not found in {$namespace} class.");
		}
		$this->_inspector = new Inspector($this->_instance);
		$methodMeta       = $this->_inspector->getMethodMeta($action);
		if (!empty($methodMeta[Annotations::$protected]) || !empty($methodMeta[Annotations::$private])) {
			throw new Exception\Action("Action '{$action} not found.");
		}

		//check acl -- --must come after the inspector is created.
		try {
			$this->_applyAccessControl($this->_instance, $namespace, $action, $methodMeta, $params);
		}
		catch (Exception\Aco $e) {

			$this->stop();
			$route = Registry::get('router')->findRoute(403);
			if (!$route) {
				throw new Exception\Route('Route `403` not found.');
			}
			Registry::set('dispatcher', new Dispatcher(['route' => $route]));
		}

		//order the url params and match to named params.
		$actionParams = $this->_inspector->getClassMethods()[$action];

		$namedParams = array();
		foreach ($actionParams as $index => $actionParam) {
			$namedParams[$index] = null;
			if (array_key_exists($actionParam, $params)) {
				$namedParams[$index] = $params[$actionParam];
			}
		}
		$this->_injector($this->_instance, $methodMeta);

		//Execute any BeforeAnnotation Actions
		$this->_hookedDispatcher($methodMeta, Annotations::$before);
		if ($this->_stop) {
			return null;
		}

		//Execute Main Action
		call_user_func_array([$this->_instance, $action], $namedParams);
		if ($this->_stop) {
			return null;
		}

		//Execute AfterAnnotation Actions
		$this->_hookedDispatcher($methodMeta, Annotations::$after);

		// if ($this->_stop) return null;

		return $this->_instance;
	}

	/**
	 * Injects services when annotated
	 *
	 * @param $instance
	 * @param $meta
	 */
	protected function _injector(&$instance, $meta)
	{
		if (isset($meta[Annotations::$service])) {
			foreach ($meta[Annotations::$service] as $service) {
				if (strpos($service, '=>') !== false) { // allow for naming
					$serviceAnnotationBreakdown = explode('=>', $service);
					$service                    = trim($serviceAnnotationBreakdown[1]);
				}

				$injectedService = new $service();

				/**
				 * Note: Annotation ACL list must match the list of services.
				 * E.g.
				 * if you pass in 2 service:
				 *
				 * @Nomad\Service    Common\Service\Message, Pickplucker\Service\League
				 *                   then you must pass in the acl actions that pair
				 * @Nomad\Acl\Action create, read
				 *                   where create corresponds with the MessageService and read is with LeagueService
				 */
				$aclAction = null;
				if (isset($meta[Annotations::$aclAction]) && !empty($meta[Annotations::$aclAction])) {
					$aclAction = 'ACL_' . strtoupper(array_shift($meta[Annotations::$aclAction]));
					$injectedService->setAclAction(constant(get_class($injectedService) . '::' . $aclAction));
				}
				else {
					$injectedService->setAclAction(null);
				}

				$instance->addService($injectedService);
			}
		}
	}

	/**
	 * @return View Return the masterView property
	 */
	public function getMasterView()
	{
		return $this->_masterView;
	}

	/**
	 * Sets the stop flag that tells a dispatcher to stop and return.
	 */
	public function stop()
	{
		$this->_stop = true;
	}

	/**
	 * Removes the stop flag
	 */
	public function unStop()
	{
		$this->_stop = false;
	}

	protected function _applyAccessControl($instantiatedClass, $namespace, $action, $methodMeta, $params = null)
	{
		$aco = Session::getSession()->aco;
		if ($aco) {

			if (!$aco->isResourceSet("{$namespace}::{$action}") && !empty($methodMeta[Annotations::$role])) {
				$aco->addResource("{$namespace}::{$action}", $methodMeta[Annotations::$role], $params);
			}

			if (!$aco->isPermitted("{$namespace}::{$action}")) {
				throw new Exception\Aco("Resource {$namespace}::{$action} is forbidden.");
			}
		}

		return true;
	}

	/**
	 * Handles the execution of any @Before, @After annotations for the dispatched method.
	 *
	 * @param $meta
	 * @param $type
	 * @return null
	 * @throws Exception\Aco
	 * @throws Exception\Route
	 */
	protected function _hookedDispatcher($meta, $type)
	{
		if (isset($meta[$type])) {
			$run = array();
			foreach ($meta[$type] as $method) {
				$matches    = StringMethods::match($method, '([a-zA-Z0-9\\\_-]+)((?=(=>)?))');
				$label      = null;
				$namespace  = null;
				$action     = array_pop($matches);
				$strictMode = isset(explode('--', $action)[1]) ? true : false;
				if ($strictMode) {
					$action = str_replace('--strict', '', $action);
				}

				if ($matches) {
					switch (count($matches)) {
						case 1:
							if (strpos($matches[0], "\\")) {
								$namespace = $matches[0];
							}
							else {
								$label     = $matches[0];
								$namespace = get_class($this->_instance);
							}
							break;
						case 2:
							$label     = $matches[0];
							$namespace = $matches[1];
							break;
						default:
					}
				}
				else {
					$namespace = get_class($this->_instance);
				}

				if ($namespace == get_class($this->_instance)) {
					//this action is in the same class/namespace, will not instantiate new
					$hookedClass = $this->_instance;
					$hookedMeta  = $this->inspector->getMethodMeta($action);
					try {
						$originalView          = $this->_instance->view;
						$viewOptions['view']   = $namespace . '::' . $action;
						$viewOptions['caller'] = $namespace . '::' . $action;
						if (!empty($hookedMeta[Annotations::$view])) {
							$viewOptions['view'] = $hookedMeta[Annotations::$view][0];
						}
						if (!empty($hookedMeta[Annotations::$layout])) {
							$viewOptions['layout'] = $hookedMeta[Annotations::$layout][0];
						}
						$this->_instance->view = new View($viewOptions);
					}
					catch (\Exception $e) {
						//there was no view file set for this hook, just unset current view.
						unset($originalView);
					}
				}
				else {
					//this action is in another class/namespace, create a new instance of it
					$hookedClass = new $namespace(
						[
							'route'  => new Route\Simple(
								array(
									'namespace' => $namespace,
									'action'    => $action
									//params?
								)),
							'caller' => $namespace . '::' . $action
						]);

					$hookedInspector = new Inspector($hookedClass);
					$hookedMeta      = $hookedInspector->getMethodMeta($action);
				}

				if (in_array($namespace . '::' . $action, $run) && !empty($hookedMeta[Annotations::$once])) {
					continue; // This is only to be run once and it has so continue.
				}

				try {
					$this->_applyAccessControl($hookedClass, $namespace, $action, $hookedMeta);
				}
				catch (Exception\Aco $e) {
					if ($strictMode) {
						$this->stop();
						$route = Registry::get('router')->findRoute(403);
						if (!$route) {
							throw new Exception\Route('Route `403` not found.');
						}
						Registry::set('dispatcher', new Dispatcher(['route' => $route]));
					}
					else {
						$label = false;
					}
				}

				$hookedClass->$action();
				if ($this->_stop) {
					return null;
				}
				if ($label) {
					if (isset($originalView)) {
						$originalView->$label  = $this->_instance->view;
						$this->_instance->view = $originalView;
					}
					else {
						$this->_instance->view->$label = $hookedClass->view;
					}
				}
				else {
					if (isset($originalView)) {
						$this->_instance->view = $originalView;
					}
				}
				$run[] = $namespace . '::' . $action;

				unset($label, $namespace, $action, $hookedClass, $hookedInspector, $hookedView, $originalView);
			}
		}
	}
}