<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;

use Nomad\Unify\StringMethods;

/**
 * Class Bootstrap
 *
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
class Bootstrap
{
	/**
	 * @var bool Default use cache
	 */
	public $useCache = true;

	/**
	 * @todo, This is still necessary?
	 * @var bool Default use mysql db
	 */
	public $useMysqlDatabase = true;

	/**
	 * @throws \Nomad\Exception\Database
	 */
	public function invoke()
	{
		$this->FatalBootstrapApplication();
		$this->initParseConfigFiles();
		$this->initRoutes();
		$this->initRequest();
		$this->initRouter();
		$this->initDispatcher();
		if ($this->useCache) {
			$this->initCache();
		}
		if ($this->useMysqlDatabase) {
			$this->initMysqlDatabase();
		}
	}

	/**
	 * Bootstraps application
	 */
	public function bootstrapApplication()
	{
		$applicationBootstrapFile = StringMethods::makePath(APPLICATION_ROOT, 'Bootstrap.php');
		if (file_exists($applicationBootstrapFile)) {
			require($applicationBootstrapFile);
		}
	}

	/**
	 * Run this before any other bootstrapping.
	 */
	public function FatalBootstrapApplication()
	{
		$applicationFatalBootstrapFile = StringMethods::makePath(APPLICATION_ROOT, 'FatalBootstrap.php');
		if (file_exists($applicationFatalBootstrapFile)) {
			require($applicationFatalBootstrapFile);
		}
	}

	/**
	 * initialized the cache
	 */
	public function initCache()
	{
		//setup cache
		try {
			//setup memcache
			$cacher = new \Nomad\Cache\Factory(['type' => 'memcached', 'inspector' => false]);
			$cacher = $cacher->initialize();
			\Nomad\Core\Registry::set('memcached', $cacher);
			defined("MEMCACHE_AVAILABLE") or define("MEMCACHE_AVAILABLE", true);

			//setup standard file cache
			$cacheLocationString = Registry::get('application')['config']->cache_location;
			$result              = preg_replace_callback(
				"({[A-Z_]+})",
				function ($matches) {
					return constant(trim($matches[0], '{}'));
				}, $cacheLocationString);
			$cacher              = new \Nomad\Cache\Factory(['type' => 'file', 'inspector' => false]);
			$cacher              = $cacher->initialize(['cacheDir' => $result]);
			\Nomad\Core\Registry::set('cache', $cacher);
			unset($cacher);

			//@TODO USE IGBINARY SILLY!
			//$ppl = igbinary_unserialize(\Nomad\Core\Registry::get('person'));

		}
		catch (\Exception $e) {
			//There is no memcache on server.
		}
	}

	/**
	 * Initializes the MySQL database (this is not the best way, I know)
	 */
	public function initMysqlDatabase()
	{
		try {
			$allConfigs = Registry::get('application');
			if (property_exists($allConfigs['config'], 'db')) {
				$dbSettings = (array)$allConfigs['config']->db;
				$db         = new \Nomad\Database\Factory((array)$dbSettings[ENVIRONMENT]);
				$db      = $db->initialize();
				\Nomad\Core\Registry::set('db', $db);
			}
		}
		catch (\Exception $e) {
			//Could not connect.
			throw new \Nomad\Exception\Database('Could not connect to datebase!!');
		}
	}

	/**
	 * Parses config files
	 *
	 * @var boolean $useSession Set to false when utilizing commandline only
	 */
	public function initParseConfigFiles($useSession = true)
	{
		$session = null;
		if ($useSession) {
			$session = \Nomad\Core\Session::getSession();
		}

		$yamlParser           = \Nomad\Configuration\Factory::create('yml');
		$allConfigs['config'] = $yamlParser->parse(\Nomad\Unify\StringMethods::makePath(APPLICATION_ROOT, 'resource', 'config', 'application.yml'));
		if ($useSession) {
			if (isset($allConfigs['config']->access_control) && !$session->aco) {
				$session->aco = new $allConfigs['config']->access_control();
			}
		}

		//Add other global configs here;
		\Nomad\Core\Registry::set('application', $allConfigs);

		\Nomad\Core\Registry::set('packages', $yamlParser->parse(\Nomad\Unify\StringMethods::makePath(APPLICATION_ROOT, 'resource', 'config', 'kernel.yml')));
	}

	/**
	 * Initializes and gets the routes
	 */
	public function initRoutes()
	{
		$allRoutes                      = array();
		$packages                       = \Nomad\Core\Registry::get('packages');
		$yamlParser                     = \Nomad\Configuration\Factory::create('yml');
		$appPackage                     = new \stdClass();
		$appPackage->namespace          = APPLICATION_DIR_NAME;
		$packages->APPLICATION_DIR_NAME = $appPackage;
		foreach ($packages as $package) {
			/**
			 * With each package
			 * - register its namespace as an alias for easy calling within other services, etc.
			 * - set package configs into registry under its config'd namespace
			 * - parse each package's route file, create route (currently only creating simple) and add to global routes
			 */
			foreach (\Nomad\Core\Package::getStandardPackageDirs() as $dir) {
				\Nomad\Core\Autoloader::registerNamespaceAlias($package->namespace . '\\' . $dir);
			}
			$isRoot = $package->namespace == APPLICATION_DIR_NAME;

			//add configs to registry
			$configArray = ['namespace' => $package->namespace];
			$configFile  = \Nomad\Unify\StringMethods::makePath(APPLICATION_ROOT, $package->namespace, 'resource', "config", strtolower($package->namespace . ".yml"));
			if ($isRoot) {
				$configFile = \Nomad\Unify\StringMethods::makePath(APPLICATION_ROOT, 'resource', "config", strtolower($package->namespace . ".yml"));
			}

			if (file_exists($configFile)) {
				$configArray['config'] = $yamlParser->parse($configFile);
			}
			\Nomad\Core\Registry::set($package->namespace, $configArray);

			//add routes (to registry too)
			$routeFile = \Nomad\Unify\StringMethods::makePath(APPLICATION_ROOT, PACKAGE_DIRECTORY, $package->namespace, 'resource', 'config', 'routes.yml');
			if ($isRoot) {
				$routeFile = \Nomad\Unify\StringMethods::makePath(APPLICATION_ROOT, 'resource', 'config', 'routes.yml');
			}
			if (file_exists($routeFile)) {
				$routes = $yamlParser->parse($routeFile);
				foreach ($routes as $name => $route) {
					$allRoutes[strtolower($name)] = new \Nomad\Router\Route\Simple($route);
				}
			}
			Registry::set('routes', $allRoutes);
		}
	}

	/**
	 * Initializes the request
	 */
	public function initRequest()
	{
		\Nomad\Core\Registry::set('request', new \Nomad\Core\Request(['rawUrl' => $_SERVER["REQUEST_URI"]]));
	}

	/**
	 * Initializes the Router
	 */
	public function initRouter()
	{
		\Nomad\Core\Registry::set(
			'router', new \Nomad\Router\Router(
			array(
				'rawUrl' => \Nomad\Core\Registry::get('request')->getRawUrl(),
				'routes' => \Nomad\Core\Registry::get('routes')
			)));
	}

	/**
	 * Initializes the dispatcher
	 */
	public function initDispatcher()
	{
		\Nomad\Core\Registry::set(
			'dispatcher', new \Nomad\Core\Dispatcher(
			[
				'route' => \Nomad\Core\Registry::get('router')->getMatchedRoute(),
			]));
	}
}