<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;

use Nomad\Exception\Annotation;
use Nomad\Unify as Unify;

/**
 * Class Inspector
 *
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
class Inspector
{
	/**
	 * @var object Current class for inspection
	 */
	protected $_class;

	/**
	 * @var array Holds inspected info
	 */
	protected $_meta = array(
		'class'      => array(),
		'properties' => array(),
		'methods'    => array(),
	);

	/**
	 * @var array
	 */
	protected $_properties;

	/**
	 * @var array
	 */
	protected $_methods;

	/**
	 * @var \ReflectionClass
	 */
	private $_reflectionClass;

	/**
	 * @var bool
	 */
	private $_canUseCache;

	/**
	 * @var bool
	 */
	private $_doUseCache = false;

	/**
	 * @var bool
	 */
	private $_doClearCache = false;

	/**
	 * @var bool
	 */
	private $_pulledFromCache;

	/**
	 * @param $class object
	 */
	public function __construct($class)
	{
		$this->_class = get_class($class);
		if ($this->_doUseCache && ($memcached = Registry::get('memcached')) && !$this->_doClearCache) {
			$this->_canUseCache = true;
			$allMeta            = unserialize($memcached->get('meta'));
			if (isset($allMeta[$this->_class])) {
				$this->_meta            = $allMeta[$this->_class];
				$this->_pulledFromCache = true;
			}
		}
		else {
			$this->_pulledFromCache = false;
		}
	}

	/**
	 * @return string
	 */
	protected function _getClassComment()
	{
		$reflection = isset($this->_reflectionClass) ? $this->_reflectionClass : new \ReflectionClass($this->_class);

		return $reflection->getDocComment();
	}

	/**
	 * @return \ReflectionProperty[]
	 */
	protected function _getClassProperties()
	{
		$reflection = isset($this->_reflectionClass) ? $this->_reflectionClass : new \ReflectionClass($this->_class);

		return $reflection->getProperties();
	}

	/**
	 * @return \ReflectionMethod[]
	 */
	protected function _getClassMethods()
	{
		$reflection = isset($this->_reflectionClass) ? $this->_reflectionClass : new \ReflectionClass($this->_class);

		return $reflection->getMethods();
	}

	/**
	 * @param $property
	 * @return string
	 */
	protected function _getPropertyComment($property)
	{

		if (property_exists($this->_class, $property)) {
			$reflection = new \ReflectionProperty($this->_class, $property);

			return $reflection->getDocComment();
		}
		return null;
	}

	/**
	 * @param $method
	 * @return string
	 */
	protected function _getMethodComment($method)
	{
		$reflection = new \ReflectionMethod($this->_class, $method);

		return $reflection->getDocComment();
	}

	/**
	 * @param $comment
	 * @return array
	 */
	protected function _parse($comment)
	{
		$meta = array();

		$pattern = "(@[a-zA-Z\\\\]+\s*[a-zA-Z0-9:|+*{}^$?<>=., \[\]\-()_\\\\]*)";
		$matches = Unify\StringMethods::match($comment, $pattern);

		if ($matches != null) {
			foreach ($matches as $match) {
				$parts           = Unify\ArrayMethods::clean(
					Unify\ArrayMethods::trim(Unify\StringMethods::split($match, "[\s]", 2))
				);
				$meta[$parts[0]] = true;
				if (count($parts) > 1) {
					$meta[$parts[0]] = Unify\ArrayMethods::clean(
						Unify\ArrayMethods::trim(Unify\StringMethods::split($parts[1], ","))
					);
				}
			}
		}

		return $meta;
	}

	/**
	 * Caches meta data
	 */
	private function _cacheMe()
	{ //change name to cacheEnabled
		if ($this->_canUseCache && $this->_doUseCache) {
			$cache   = Registry::get('memcached');
			$allMeta = unserialize($cache->get('meta'));

			$allMeta[$this->_class] = $this->_meta;
			$cache->set('meta', serialize($allMeta));
		}
	}

	/**
	 * @return array
	 */
	public function getClassProperties()
	{
		if (!isset($this->_properties)) {
			$properties = $this->_getClassProperties();
			foreach ($properties as $property) {
				$this->_properties[] = $property->getName();
			}
		}

		return $this->_properties;
	}

	/**
	 * @return array
	 */
	public function getClassMethods()
	{
		if (!isset($this->_methods)) {
			$methods = $this->_getClassMethods();
			foreach ($methods as $method) {
				$name = $method->getName();
				if ($method->isPublic() && !$method->isConstructor() && !in_array(
						$name, [
						'__call',
						'__get',
						'__set',
						'__construct',
						'__destruct'
					])
				) {
					$params     = $method->getParameters();
					$paramNames = array();
					foreach ($params as $param) {
						$paramNames[] = $param->getName();
					}
					$this->_methods[$name] = $paramNames;
				}
				else {
					$this->_methods[$name] = "PRIVATE";
				}
			}
		}

//$this->_cacheMe();
		return $this->_methods;
	}

	/**
	 * Gets class annotations
	 *
	 * @return array|null
	 */
	public function getClassMeta()
	{
		if (empty($this->_meta['class'])) {
			$comment = $this->_getClassComment();
			if (!empty($comment)) {
				$this->_meta['class'] = $this->_parse($comment);
			}
			else {
				$this->_meta['class'] = null;
			}
		}

		//$this->_cacheMe();
		return $this->_meta['class'];
	}

	/**
	 * Gets property annotations
	 *
	 * @param $property
	 * @return mixed
	 */
	public function getPropertyMeta($property)
	{
		if (!isset($this->_meta['properties'][$property])) {

			$comment = $this->_getPropertyComment($property);

			if (!empty($comment)) {
				$this->_meta['properties'][$property] = $this->_parse($comment);
			}
			else {
				$this->_meta['properties'][$property] = null;
			}
		}

//        $this->_cacheMe();
		return $this->_meta['properties'][$property];
	}

	/**
	 * Gets method annotations
	 *
	 * @param $method
	 * @return mixed
	 */
	public function getMethodMeta($method)
	{
		if (!isset($this->_meta['methods'][$method])) {
			$comment = $this->_getMethodComment($method);
			if (!empty($comment)) {
				$this->_meta['methods'][$method] = $this->_parse($comment);
			}
			else {
				$this->_meta['methods'][$method] = null;
			}
		}

		//$this->_cacheMe();
		return $this->_meta['methods'][$method];
	}

	/**
	 * Generates a mapper for an model using @Nomad\Db\Table, and other annotated properties
	 * @return bool
	 * @throws Annotation
	 */
	public function generateMapper()
	{
		$mapper = false;

		$classMeta = $this->getClassMeta();
		if (isset($classMeta[\Nomad\Core\Annotations::$mapperDbTable])) {
			/**
			 * The class must declare a table to use in order for the generate mapper to proceed.
			 */
			$mapper['table'] = $classMeta[\Nomad\Core\Annotations::$mapperDbTable][0];
			$allProperties = $this->getClassProperties();
			$jsonProperties = array();
			foreach ($allProperties as $property) {
				$comments = $this->getPropertyMeta($property);
				$mapperAnnotations = array_intersect(\Nomad\Core\Annotations::getMapperDbAnnotations(), array_keys($comments));
				if (count($mapperAnnotations) === 0){
					continue;
				}elseif (count($mapperAnnotations) === 1){
					$annotation = array_shift($mapperAnnotations);
					switch ($annotation) {
						case \Nomad\Core\Annotations::$mapperDbColumn:
							$mapper['properties'][$comments[$annotation][0]] = "%{$property}%" ;
							break;
						case \Nomad\Core\Annotations::$mapperDbIgnore:
							break;
						case \Nomad\Core\Annotations::$mapperDbJson:

							$toJsonArray = explode('.', $comments[$annotation][0]);
							if (count($toJsonArray) < 2) {
								throw new Annotation("`@Nomad\\Db\\Json expects property separated by `.`");
							}
							$dbCol = array_shift($toJsonArray);
							$jsonProperties[$dbCol][$property] = implode('.', $toJsonArray);
							break;
						case \Nomad\Core\Annotations::$mapperDbFunction:
							break;
					}
				}else{
					throw new Annotation("You may only use one @Nomad\\Db\\... annotation to describe the {$property} property");
				}
			}
			if ($jsonProperties) {
				$theArray = [];
				foreach ($jsonProperties as $property => $strings) {
					$theArray[$property]= $this->_parseJsonAnnotationArray($strings);
				}
				$mapper['properties'] = array_merge($mapper['properties'], $theArray);
			}

			return $mapper;

		}
	}

	/**
	 * Converts dot notation to array
	 * ex fruits.florida.oranges  becomes ['fruit' => ['florida' => ['oranges'=>$value] ] ]
	 *
	 * @param $string
	 * @return array
	 */
	protected function _parseJsonAnnotationArray(array $stringArray, $workingArray = array())
	{
		foreach ($stringArray as $property=>$string) {
			$exploded = explode('.', $string);
			if (count($exploded) < 2) {
				$workingArray[$exploded[0]] = "%{$property}%";
			}else{
				$thisProperty = array_shift($exploded);
				if (isset($workingArray[$thisProperty])) {
					$workingArray[$thisProperty] = array_merge_recursive($workingArray[$thisProperty],$this->_parseJsonAnnotationArray([$property=>implode('.',$exploded)] ));
				}else{
					$workingArray[$thisProperty] = $this->_parseJsonAnnotationArray([$property=>implode('.',$exploded)]);
				}
			}
		}

		return  $workingArray;
	}
}