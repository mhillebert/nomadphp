<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;
/**
 * Class Package
 * Class holder for standard package information.
 *
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
class Package
{
	/**
	 * Names of the directories for the appropriate classes
	 *
	 * @var array
	 */
	protected static $_standardPackageDirs = array(
		'Controller',
		'Model',
		'Service',
	);

	/**
	 * @return array
	 */
	static public function getStandardPackageDirs()
	{
		return self::$_standardPackageDirs;
	}
}