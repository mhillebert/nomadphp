<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;
/**
 * Annotations used in Nomad.
 * just a simple file that contains the annotation definitions
 *
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
class Annotations
{
	/**
	 * The Nomad\Type Annotation
	 * params:
	 *  boolean|integer|double|string|array|object|resource|NULL|unknown type
	 *
	 * description:
	 *      delare a property to be a certain type; namely a php defined type returned by gettype
	 *      see: http://www.php.net/manual/en/function.gettype.php
	 *
	 * @var string
	 */
	public static $type = '@Nomad\Type';

	/**
	 * The known Nomad Validator types for shorter annotations
	 *
	 * @var array
	 */
	public static $knownNomadValidatorTypesArray = array(
		'Digit',
		'Regex'
	);
	/**
	 * The Nomad\Validator Annotation
	 * params: string
	 *      will accept any param inside parenthesis and pass the new value and that param to your method.
	 *
	 * use:
	 *     (1) Use a predefined Nomad Validator:
	 *          eg.
	 *          The annotation will look like this:
	 *
	 * @Nomad\Validator Digit       or
	 * @Nomad\Validator Regex((a|A)\b|([aA]n)|([tT]he)\b)     \\match a|A, an|An, the|The words
	 *
	 *      (2) You may implement the Nomad\Validator\Validator_Interface on your own class,
	 *          and the static ::isValid method will be called and passed the value and the annotation param.
	 *          eg.
	 *              namespace My\Namespace;
	 *              class MyClass implements \Nomad\Validator\Validator_Interface
	 *              {
	 *                  public static function isValid($value, $theParameterFromAnnotation)
	 *                  {
	 *                      //do something
	 *                      return true|false
	 *                  }
	 *              }
	 *          and the annotation will look like this:
	 * @Nomad\Validator My\Namespace\MyClass(thisIsAParameter)
	 *
	 *      (3) You may use you own static method within your own class:
	 *          namespace My\Namespace;
	 *              class MyClass
	 *              {
	 *                  public static function theValidator($value, $theParameterFromAnnotation)
	 *                  {
	 *                      //do something
	 *                      return true|false
	 *                  }
	 *              }
	 *          and the annotation will look like this:
	 * @Nomad\Validator My\Namespace\MyClass::theValidator(thisIsAParameter)
	 *
	 *  or  (4) You may use you own public method within your own class:
	 *          namespace My\Namespace;
	 *              class MyClass
	 *              {
	 *                  public function customValidator($value, $theParameterFromAnnotation)
	 *                  {
	 *                      //do something
	 *                      return true|false
	 *                  }
	 *              }
	 *          and the annotation will look like this:
	 * @Nomad\Validator My\Namespace\MyClass->customValidator(thisIsAParameter)
	 *
	 * failed:
	 *  Failed validations will be given as an array by calling the $yourClass->getFailedValidation(). Of course,
	 *  this assumes you have extended the Nomad\Core\BaseClass otherwise none of this applies.
	 *  The array will be indexed values of normalized property names (without the underscore).
	 *
	 * @var string
	 */
	public static $validator = '@Nomad\Validator';

	/**
	 * The Nomad\readOnly Annotation
	 * params: none
	 * use:
	 *
	 * @Nomad\readOnly
	 *
	 * description:
	 *      mark a property as read-only. Will throw an Nomad\Exception\PropertyAccess if write is attempted.
	 * @var string
	 */
	public static $readOnly = '@Nomad\ReadOnly';

	/**
	 * The Nomad\writeOnly Annotation
	 * params: none
	 * use:
	 *
	 * @Nomad\writeOnly
	 *
	 * description:
	 *      mark a property as write-only. Will throw an Nomad\Exception\PropertyAccess if read is attempted.
	 *
	 * @var string
	 */
	public static $writeOnly = '@Nomad\WriteOnly';

	/**
	 * Execute only once
	 *
	 * @var string
	 */
	public static $once = '@Nomad\Once';

	/**
	 * A protected property
	 *
	 * @var string
	 */
	public static $protected = '@Nomad\Protected';

	/**
	 * A private property
	 *
	 * @var string
	 */
	public static $private = '@Nomad\Private';

	/**
	 * A comma delimited list of methods to execute before the called method.
	 * Proper construction:
	 * @Nomad\Before [associationLabelName=>(optional)]Full\Namespaced\Class::method[--strict(optional)]
	 *
	 * You may add an association to the class::method string to set the called method's view to the associated name.
	 * Without the association, the method will still execute (unless strict flag is set...see below) but no view is
	 * passed back.
	 * E.g. with association:
	 *
	 * @Nomad\Before auth=>Common\Service\Authentication::authenticate
	 *               The above line will set the authenticate's view into the $this->view->auth variable to be used within the calling
	 *               methods view.
	 *
	 * E.g. without association:
	 * @Nomad\Before Common\Service\Authentication::authenticate
	 *
	 * Also, you can alter the behavior with the '--strict' flag.
	 * Adding this flag at the end of the will redirect to a 403 page if the role is not permitted;
	 * otherwise the method will be skipped.
	 * E.g. without strict:
	 * @Nomad\Before auth=>Common\Service\Authenticate::authenticate     (or)
	 * @Nomad\Before Common\Service\Authenticate::authenticate
	 * This method will not be executed, nor set as a view variable.
	 *
	 * E.g. with strict:
	 * @Nomad\Before auth=>Common\Service\Authenticate::authenticate--strict     (or)
	 * @Nomad\Before Common\Service\Authenticate::authenticate--strict
	 * This method will not be executed, nor set as a view variable AND redirected immediately to a 403.
	 *
	 * Same for @Nomad\After; except the method runs after the called method.
	 * @var string
	 */
	public static $before = '@Nomad\Before';
	public static $after = '@Nomad\After';

	/**
	 * Designates as event for event system
	 *
	 * @var string
	 */
	public static $event = '@Nomad\Event';

	/**
	 * The parameter needs to look like a namespace that resembles where the file is located - ignoring lower-cased directory names.
	 * E.g.
	 *
	 * @Nomad\Layout funWithNomad will look for the file at APPLICATION_ROOT/layout/funWithNomad.phtml (Note: 0 backslashes, 0 double-colon)
	 * @Nomad\Layout Common::funWithNomad will look for the file at APPLICATION_ROOT/package/Common/layout/funWithNomad.phtml (Note: 0 backslashes)
	 * @Nomad\Layout Common\Service::meow will look for the file at APPLICATION_ROOT/package/Common/Service/layout/meow.phtml (Note: 1 backslash)
	 * @Nomad\Layout Common\Controller\NomadUser::gateway will look for the file at APPLICATION/package/Common/Controller/NomadUser/layout/gateway.phtml (Note: 2 backslashes)
	 * @var string
	 */
	public static $layout = '@Nomad\Layout';

	/**
	 * View to use
	 *
	 * @var string
	 */
	public static $view = '@Nomad\View';

	/**
	 * Roles allowed to execute method.
	 * This is a comma delimited string of roles. If roles are set-up properly in the tree, then only the lowest role
	 * permitted is needed.
	 *
	 * @var string
	 */
	public static $role = '@Nomad\Role';

	/**
	 * Use assert
	 *
	 * @var string
	 */
	public static $assert = '@Nomad\Assert';

	/**
	 * Inject service
	 *
	 * @var string
	 */
	public static $service = '@Nomad\Service';

	/**
	 *  Acl action
	 *
	 * @var string
	 */
	public static $aclAction = '@Nomad\Acl\Action';
	public static $aclRead = '@Nomad\Acl\Read';
	public static $aclCreate = '@Nomad\Acl\Create';
	public static $aclUpdate = '@Nomad\Acl\Update';
	public static $aclDelete = '@Nomad\Acl\Delete';
	public static $aclOwnerProperty = '@Nomad\Acl\OwnerProperty';
	public static $aclUserProperty = '@Nomad\Acl\UserProperty';

	/**
	 * Database mapper
	 * @var string
	 */
	public static $mapperDbTable = '@Nomad\Db\Table';
	public static $mapperDbColumn = '@Nomad\Db\Column';
	public static $mapperDbJson = '@Nomad\Db\Json';
	public static $mapperDbFunction = '@Nomad\Db\Function';
	public static $mapperDbIgnore = '@Nomad\Db\Ignore';

	/**
	 * Returns all available mapperDb Annotations
	 * @return array
	 */
	public static function getMapperDbAnnotations() {
		return array(
			self::$mapperDbTable, self::$mapperDbColumn, self::$mapperDbJson, self::$mapperDbFunction, self::$mapperDbIgnore
		);
	}
}