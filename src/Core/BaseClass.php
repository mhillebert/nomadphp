<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;

use Nomad\Unify as Unify;
use Nomad\Exception as Exception;
use function is_array;

/**
 * Class BaseClass
 * Hold the 'magic' for properties.
 *
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
class BaseClass
{
	/**
	 * @var Inspector
	 */
	protected $_inspector;

	/**
	 * Holds the property=>oldValue array for properties that changed
	 * Must be protected not private to carry over in sessions.
	 *
	 * @var array
	 */
	protected $_changedProperties;

	/**
	 * Invalid properties array
	 * property => expected type
	 *
	 * @var array
	 */
	private $_propertyTypeMismatches;

	/**
	 * Properties that have failed validation
	 *
	 * @var array
	 */
	private $_failedValidations;

	/**
	 * These will be ignored when changed
	 *
	 * @var array
	 */
	private $__ignorePropertyChanges = array(
		'services',
		'inspector',
		'ignorePropertyChanges',
		'mergePropertyCallback'
	);

	/**
	 * Callback function for merges
	 *
	 * @var array
	 */
	protected $_mergePropertyCallback = array(
		'changedProperties' => 'array_merge'
	);

	/**
	 * @param array $options
	 */
	public function __construct($options = array())
	{
		if (!isset($options['inspector']) || (isset($options['inspector']) && $options['inspector'] === true)) {
			$this->_inspector = new Inspector($this);
		}
		else {
			$this->_inspector = false;
		}

		if (is_array($options) || is_object($options)) {
			foreach ($options as $key => $value) {
				$method = "set" . ucfirst($key);
				$this->$method($value);
			}
		}
	}

	/**
	 * Magic method for properties
	 *
	 * @param $name
	 * @param $arguments
	 * @throws \Nomad\Exception\PropertyAccess
	 * @throws \Nomad\Exception\BaseClass
	 * @throws \Nomad\Exception\MethodIsNotImplemented
	 * @return $this|null
	 */
	public function __call($name, $arguments)
	{
		if (empty($this->_inspector) && $this->_inspector !== false) {
			throw new Exception\BaseClass();
		}
		//GET PROPERTY
		$getMatches = Unify\StringMethods::match($name, "^get([a-zA-Z0-9]+)$");
		if (is_array($getMatches) && count($getMatches) > 0) {
			$normalized = lcfirst($getMatches[0]);
			$property   = "_{$normalized}";
			if (!property_exists($this, $property)) {
				throw new Exception\PropertyAccess("'{$normalized}'' property is invalid.");
			}
			if ($this->_inspector !== false) {
				$meta = $this->_inspector->getPropertyMeta($property);
				$this->__checkWriteOnlyAnnotation($meta, $normalized);
			}
			//CHECK THE ANNOTATIONS
			if (isset($this->$property)) {
				return $this->$property;
			}

			return null;
		}
		//SET PROPERTY
		$setMatches = Unify\StringMethods::match($name, "^set([a-zA-Z0-9]+)$");
		if (count($setMatches) > 0) {
			$normalized = lcfirst($setMatches[0]);
			$property   = "_{$normalized}";
			if (!property_exists($this, $property)) {
				throw new Exception\PropertyAccess("{$normalized} property is invalid.");
			}
			if ($this->_inspector !== false) {
				//CHECK THE ANNOTATIONS
				$meta = $this->_inspector->getPropertyMeta($property);
				$this->__checkReadOnlyAnnotation($meta, $normalized);
				$this->__checkTypeAnnotation($meta, $normalized, $arguments[0]);
				$this->__checkValidatorAnnotation($meta, $normalized, $arguments[0]);
			}
			//ADD TO CHANGED ARRAY only if the value has changed
			if ($this->$property !== $arguments[0] && !in_array($normalized, $this->__ignorePropertyChanges)) {
				$this->_changedProperties[$normalized] = $this->$property;
			}

			//SET THE NEW PROPERTY VALUE ON IF CORRECT TYPE AND VALID
			if (!$this->anyPropertyFailedValidation() && !$this->anyPropertyTypeMismatches()) {
				$this->$property = $arguments[0];
			}

			return $this;
		}
	}

	/**
	 * Magic get. Creates a function and calls it, which redirects to the magic call.
	 *
	 * @param $name
	 * @return mixed
	 */
	public function __get($name)
	{
		//throw new \Exception("USE get" . ucfirst($name) . "()");
		$function = "get" . ucfirst($name);

		return $this->$function();
	}

	/**
	 * Magic set. Creates a function and calls it, which redirects to the magic call.
	 *
	 * @param $name
	 * @param $value
	 * @return mixed
	 */
	public function __set($name, $value)
	{
		$function = "set" . ucfirst($name);

		return $this->$function($value);
	}

	/**
	 * Returns the array of properties with type mismatches set by Annotations
	 * property => expecting type
	 *
	 * @return array
	 */
	public function getPropertyTypeMismatches()
	{
		return $this->_propertyTypeMismatches;
	}

	/**
	 * Returns the array of properties that failed validation set by Annotation
	 * property => expecting type
	 *
	 * @return array
	 */
	public function getFailedValidations()
	{
		return $this->_failedValidations;
	}

	/**
	 * Answers this Question: Did any property not pass the annotated Type test?
	 *
	 * @return bool
	 */
	public function anyPropertyTypeMismatches()
	{
        return is_array($this->_propertyTypeMismatches) && count($this->_propertyTypeMismatches);
    }

	/**
	 * Answers this Question: Did any property not pass the annotated Validator test?
	 *
	 * @return bool
	 */
	public function anyPropertyFailedValidation()
	{
        return is_array($this->_failedValidations) && count($this->_failedValidations);
    }

	/**
	 * Returns an associative array of the properties that have been changed since creation of class.
	 * The value of the array would be the PREVIOUS value.
	 *
	 * @return mixed
	 */
	public function getChanged()
	{
		return $this->_changedProperties;
	}

	/**
	 * Resets the changedProperties.
	 *
	 * @return mixed
	 */
	public function resetAllChanges()
	{
		$this->_changedProperties = array();
	}

	/**
	 * @param $object
	 * @throws Exception\Argument
	 */
	public function mergeFrom($object)
	{
		if (get_class($object) != get_class($this)) {
			throw new Exception\Argument("mergeFrom expects the same object type: " . get_class(($this)));
		}

		foreach ($object as $propertyName => $propertyValue) {
			$normalizedPropertyName = ltrim($propertyName, '_');
			if (!in_array($normalizedPropertyName, $this->__ignorePropertyChanges)) {
				$propertyNameGet = 'get' . $normalizedPropertyName;
				$propertyNameSet = 'set' . $normalizedPropertyName;
				if (isset($this->_mergePropertyCallback[$normalizedPropertyName])) {

					call_user_func(
						array(
							$this,
							$this->_mergePropertyCallback[$normalizedPropertyName]
						), $object->$propertyNameGet());

					if ($this->$propertyNameGet() != $object->$propertyNameGet()) {
						if ($normalizedPropertyName != 'changedProperties') {
							$this->_changedProperties[$normalizedPropertyName] = "";
						}
					}
				}
				else {
					if (!is_null($object->$propertyNameGet())) {
						if ($this->$propertyNameGet() != $object->$propertyNameGet()) {
							//only set the properties that have changed, and update the changed properties array
							$this->$propertyNameSet($object->$propertyNameGet());
							$this->_changedProperties[$normalizedPropertyName] = $this->$propertyNameGet();
						}
					}
				}
			}
		}
	}

	/**
	 * Internal helper function
	 *
	 * @param $meta
	 * @param $normalized
	 * @param $value
	 */
	private function __checkTypeAnnotation($meta, $normalized, $value)
	{
		if ($meta) {
			$propertyType = gettype($value);
			if (isset($meta[Annotations::$type]) && !in_array($propertyType, $meta[Annotations::$type])) {
				$this->_propertyTypeMismatches[$normalized] = implode(', ', $meta[Annotations::$type]);
			}
		}
	}

	/**
	 * Internal helper function
	 *
	 * @param $meta
	 * @param $normalized
	 * @throws \Nomad\Exception\PropertyAccess
	 */
	private function __checkReadOnlyAnnotation($meta, $normalized)
	{
		if (isset($meta[Annotations::$readOnly]) && $meta[Annotations::$readOnly] !== false) {
			throw new Exception\PropertyAccess("{$normalized} is read-only.");
		}
	}

	/**
	 * Internal Helper function
	 *
	 * @param $meta
	 * @param $normalized
	 * @throws \Nomad\Exception\PropertyAccess
	 */
	private function __checkWriteOnlyAnnotation($meta, $normalized)
	{
		if (isset($meta[Annotations::$writeOnly]) && $meta[Annotations::$writeOnly] !== false) {
			throw new Exception\PropertyAccess("{$normalized} is write-only.");
		}
	}

	/**
	 * Internal Helper function
	 *
	 * @param $meta
	 * @param $normalized
	 * @param $value
	 * @throws \Nomad\Exception\Annotation
	 */
	private function __checkValidatorAnnotation($meta, $normalized, $value)
	{
		//not sure if i want to keep this in here; may not be the best, especially with multiple errors on a form?
		//::Created properties to check to add for failed validation and type mismatches.
		//::the programmer should check and make use of these arrays
		if (isset($meta[Annotations::$validator])) {
			$params = Unify\StringMethods::match($meta[Annotations::$validator][0], "~\((.*)\)~");
			if (count($params) > 0) {
				$params = $params[0];
			}
			else {
				$params = null;
			}
			//determine what type of function is being passed
			$validationClassAndMethod = str_replace("($params)", "", $meta[Annotations::$validator][0]);
			if (in_array($validationClassAndMethod, Annotations::$knownNomadValidatorTypesArray)) {
				//this annotation is a known Nomad Validator
				//check if Regex
				$method = "Nomad\\Validator\\" . $validationClassAndMethod;
				if (!$method::isValid($value, $params)) {
					$this->_failedValidations[] = $normalized;
				}
			}
			else {
				preg_match("/(::)|(->)/", $validationClassAndMethod, $customMethod);
				if (count($customMethod) == 0) {
					if (class_exists($validationClassAndMethod) && class_implements($validationClassAndMethod) == 'Nomad\Validator\Validator_Interface') {
						if (!$validationClassAndMethod::isValid($value, $params)) {
							$this->_failedValidations[] = $normalized;
						}
					}
					else {
						throw new Exception\Annotation();
					}
				}
				else {
					switch ($customMethod[0]) {
						case '::': //static method
							$classMethodParts = explode('::', $validationClassAndMethod);
							if (!$classMethodParts[0]::$classMethodParts[1]($value, $params)) {
								$this->_failedValidations[] = $normalized;
							}
							break;
						case '->': //class method
							$classMethodParts = explode('->', $validationClassAndMethod);
							if (!$classMethodParts[0]::$classMethodParts[1]($value, $params)) {
								$this->_failedValidations[] = $normalized;
							}
							break;
						default:
							throw new Exception\Annotation(
								"You must use a Nomad Implemented Validator,
                            provide the Namespace\\Class::Static, or Namespace\\Class->Method syntax
                            in the validator annotation");
					}
				}
			}
		}
	}
}