<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;
/**
 * Class Registry
 * A typical registry class to store object instances
 *
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
class Registry
{
	/**
	 * @var array
	 */
	private static $_instances = array();

	/**
	 * Prevent new classes
	 */
	private function __construct()
	{
	}

	/**
	 * Clone r
	 */
	private function __clone()
	{
	}

	/**
	 * Get instance from registry
	 *
	 * @param      $key
	 * @param null $default
	 * @method static \Nomad\Router\Router get($key)
	 * @return null
	 */
	public static function get($key, $default = null)
	{
		if (isset(self::$_instances[$key])) {
			return self::$_instances[$key];
		}

		return $default;
	}

	/**
	 * Set instance in registry
	 *
	 * @param      $key
	 * @param null $instance
	 */
	public static function set($key, $instance = null)
	{
		self::$_instances[$key] = $instance;
	}

	/**
	 * Removes instance from registry
	 *
	 * @param $key
	 */
	public static function erase($key)
	{
		unset(self::$_instances[$key]);
	}

	/**
	 * @TODO remove?
	 * @return array
	 */
	public static function getAllInstances()
	{
		return self::$_instances;
	}
}