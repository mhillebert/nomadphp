<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;
/**
 * Class EventProxy
 *
 * @TODO    Currently not in use
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
class EventProxy
{
	/**
	 * @var null
	 */
	protected $_events;

	/**
	 * @param       $className
	 * @param array $options
	 */
	public function __construct($className, $options = array())
	{
		$reflection = new \ReflectionClass($className);
		if ($this->_events = \Nomad\Core\Registry::get('Events')) {
			$methods = $reflection->getMethods(\ReflectionMethod::IS_PUBLIC);
			foreach ($methods as $method) {
				$methodReflected = $reflection->getMethod($method->getName());
				$methodMeta      = $methodReflected->getDocComment();
				if (strstr($methodMeta, Annotations::$event)) {
					$this->_events->addEvent($className, $method->getName());
				}
			}
		}
	}

	/**
	 * @param $name
	 * @param $arguments
	 */
	public function __call($name, $arguments)
	{
		if (is_array($this->_events->_preAct) && array_key_exists($fullClassAndMethodName, $this->_events->_preAct)) {
			foreach ($this->_events->_preAct[$fullClassAndMethodName] as $action) {
				$parts    = explode(':', $action);
				$class    = $parts[0];
				$method   = $parts[1];
				$fullName = "{$class}:{$method}";
				if (is_callable($parts[0])) {
					echo "callable";
				}
				else {
					if (!in_array($fullName, $this->_events->_eventsAvailable)) {
						$this->_events->_eventsAvailable[] = $fullName;
					}
					$newInstance = new $class();
					//Registry::set($parts[0], new $parts[0]);
					$newInstance->$method();
				}
			}
		}
	}
}