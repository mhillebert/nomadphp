<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;
/**
 * Class Session
 *
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
class Session
{
	/**
	 * State of session
	 */
	const SESSION_STARTED = true;
	const SESSION_NOT_STARTED = false;

	/**
	 * Number of seconds a Nomad_Session can live
	 */
	const DEFAULT_SESSION_LIFETIME = 7200; //2 hour;

	/**
	 * @var string Additional security
	 */
	const SALT = '=&^$*NOMAD*$^&=';

	/**
	 * @var int NomadUser set session lifetime in seconds
	 */
	public static $sessionLifetime;

	/**
	 * @var int Time the session was started.
	 */
	protected $_status = self::SESSION_NOT_STARTED;

	/**
	 * @var int Time the session was last accessed.
	 */
	protected $_lastAccessedTime;

	/**
	 * @var string Id of the session
	 */
	protected $_id;

	/**
	 * @var \Nomad\Core\Session object
	 */
	protected static $_instance;

	/**
	 * @var array stores variables assigned to Nomad Session
	 */
	protected $_store;

	/**
	 * Add a tiny bit more security.
	 * a hash of the user agent.
	 *
	 * @var string $_SERVER ['HTTP_USER_AGENT']
	 */
	protected $_userAgent;

	/**
	 * function to get the instantiated session object or
	 * create a new one (singleton pattern)
	 */
	public static function getSession()
	{
//        if (!empty(Nomad_Application::$sessionDatabase)){
//            $sessionHandler = new Nomad_DatabaseSessionHandler();
//            /**
//             * Change the save_handler to use
//             * the class functions
//             */
//            session_set_save_handler (
//                array($sessionHandler, 'open'),
//                array($sessionHandler, 'close'),
//                array($sessionHandler, 'read'),
//                array($sessionHandler, 'write'),
//                array($sessionHandler, 'destroy'),
//                array($sessionHandler, 'gc')
//            );
//        }

		if (isset($_SERVER['SHELL'])) {
			return false;
		}

		if (!isset(self::$_instance)) {
			self::$_instance = new self;
		}

		self::$_instance->startSession();

		return self::$_instance;
	}

	/**
	 * Begins a session if one does not exist or restores one of it does.
	 */
	public function startSession()
	{
		if ($this->_status == self::SESSION_NOT_STARTED && !isset($_SESSION)) {
			ini_set('session.use_only_cookies', true);
			/* USE HTTPS ONLY */ //ini_set('session.cookie_secure', true);
			ini_set('session.cookie_httponly', true);
			ini_set('session.cookie_lifetime', self::DEFAULT_SESSION_LIFETIME);
			ini_set('session.gc_maxlifetime', self::DEFAULT_SESSION_LIFETIME);

			session_name('NOMAD');
			session_start();
			$this->_status = self::SESSION_STARTED;

			if (isset($_SESSION['Nomad_Session'])) {
				$this->_restoreSession();
			}
			else {
				$this->_createSession();
			}
		}
		else {
			$this->_restoreSession();
		}
	}

	/**
	 * Regenerate the id of current session. Useful when user changes roles.
	 */
	public function regenerateId()
	{
		session_regenerate_id(true);

		$this->_id = $_SESSION['Nomad_Session']->_id = session_id();

		return $this;
	}

	/**
	 * Restore the session info to self::
	 */
	protected function _restoreSession()
	{
		$this->_id               = $_SESSION['Nomad_Session']->_id;
		$this->_lastAccessedTime = $_SESSION['Nomad_Session']->_lastAccessedTime;
		$this->_userAgent        = $_SESSION['Nomad_Session']->_userAgent;

		if ($this->_validateSession()) {
			$this->_store            = $_SESSION['Nomad_Session']->_store;
			$this->_lastAccessedTime = $_SESSION['Nomad_Session']->_lastAccessedTime = time();
		}
		else {
			$this->resetSession();
		}
	}

	/**
	 * Creates the necessary variables and assigns to session
	 */
	protected function _createSession()
	{
		session_regenerate_id(true);
		$this->_id               = session_id();
		$this->_lastAccessedTime = time();
		$this->_status           = self::SESSION_STARTED;
		//we remove FirePHP modified header
		$this->_userAgent          = md5(preg_replace("/ FirePHP\/\d\.\d\.\d/i", '', $_SERVER['HTTP_USER_AGENT']) . self::SALT);
		$_SESSION['Nomad_Session'] = $this;
	}

	/**
	 * Determines if the Session is still valid
	 *
	 * @return bool
	 */
	protected function _validateSession()
	{
		$sessionLifetime = isset(self::$sessionLifetime) ? self::$sessionLifetime : self::DEFAULT_SESSION_LIFETIME;

		/** Check Http_user_agent */
		//we remove FirePHP modified header
		if ($this->_userAgent == md5(preg_replace("/ FirePHP\/\d\.\d\.\d/i", '', $_SERVER['HTTP_USER_AGENT']) . self::SALT) &&
			$this->getAge() <= $sessionLifetime
		) {
			return true;
		}
		else {

			return false;
		}
	}

	/**
	 * Terminates and un-sets session variables
	 */
	public function endSession()
	{
		$this->_status = self::SESSION_NOT_STARTED;
		unset($_SESSION['Nomad_Session']);
		unset($_SESSION);
		$params = session_get_cookie_params();
		setcookie(
			session_name(), '', time() - 42000,
			$params["path"], $params["domain"],
			$params["secure"], $params["httponly"]
		);
		setcookie("PHPSESSID", "", time() - 3600, "/");
		session_destroy();
		self::$_instance = null;
		$this->_createSession();
	}

	/**
	 * Ends the current session and begins a new one.
	 */
	public function resetSession()
	{
		$this->endSession();
		$this->startSession();

		return $this;
	}

	/**
	 * @param $property
	 * @param $value
	 */
	public function __set($property, $value)
	{
		$this->_store[$property]   = $value;
		$_SESSION['Nomad_Session'] = $this;
	}

	/**
	 * @param $property
	 * @return null
	 */
	public function __get($property)
	{
		if (isset($this->_store[$property])) {
			return $this->_store[$property];
		}

		return null;
	}

	/**
	 * @param $name
	 * @return bool
	 */
	public function __isset($name)
	{
		return isset($this->_store[$name]);
	}

	/**
	 * Unset session variable
	 *
	 * @param $name
	 */
	public function __unset($name)
	{
		unset($_SESSION['Nomad_Session']->$name, $this->_store[$name]);
	}

	/**
	 * Determines the age (in seconds) of the current session.
	 *
	 * @return int
	 */
	public function getAge()
	{
		return time() - $this->_lastAccessedTime;
	}

	/**
	 * @return string Gets session id
	 */
	public function getId()
	{
		return session_id();
	}

	/**
	 * @return int
	 */
	public function getdateLastAccessed()
	{
		return $this->_lastAccessedTime;
	}
}