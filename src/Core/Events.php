<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;
/**
 * Class Events
 *
 * @TODO    Currently not in use.
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
class Events
	extends BaseClass
{
	/**
	 * @var array
	 */
	protected $_eventsAvailable = array();

	/**
	 * @var
	 */
	protected $_preAct;

	/**
	 * @var
	 */
	protected $_reAct;

	/**
	 * @param $class
	 * @param $method
	 */
	public function addEvent($class, $method)
	{
		$fullClassAndMethodName = $class . "::" . $method;
		if (!in_array($fullClassAndMethodName, $this->_eventsAvailable)) {
			$this->_eventsAvailable[] = $fullClassAndMethodName;
		}
	}

	/**
	 * @return array
	 */
	public function getEventsAvailable()
	{
		return $this->_eventsAvailable;
	}

	/**
	 * @param $event
	 * @param $action
	 */
	public function preAct($event, $action)
	{
		$this->_preAct[$event][] = $action;
	}
}