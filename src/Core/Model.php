<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;

use Nomad\Exception\Service;

/**
 * Class Model
 *
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
class Model
	extends BaseClass
{
	/**
	 * Holds services for lazy loading
	 *
	 * @var array mixed
	 */
	protected $_services;

	/**
	 * @param array $options
	 */
	public function __construct($options = array())
	{
		parent::__construct($options);

		if ($this->_inspector) {
			$meta = $this->_inspector->getClassMeta();

			if (isset($meta[Annotations::$service])) {
				foreach ($meta[Annotations::$service] as $service) {
					//Were not instantiating the service until its called
					$serviceParts                              = explode('\\', $service);
					$this->_services[array_pop($serviceParts)] = $service;
				}
			}
		}
	}

	/**
	 * Handles Service calls if call contains _get{someService}Service;
	 *
	 * @param $name
	 * @param $arguments
	 * @return $this|null
	 * @throws \Nomad\Exception\Service
	 */
	public function __call($name, $arguments)
	{

		$getMatches = \Nomad\Unify\StringMethods::match($name, "^_get([a-zA-Z0-9]+)Service$");
		if (count($getMatches) > 0) {
			//add a slash before each capital letter (except first)
			$class = str_replace('\\', '', preg_replace('/(\w+)([A-Z])/U', '\\1\\\\\2', $getMatches[0]));
			if ($this->_services && array_key_exists($class, $this->_services)) {
				if (is_string($this->_services[$class])) {
					$this->_services[$class] = new $this->_services[$class]();
				}
				if ($arguments) {
					if (!is_array($arguments[0])) {
						throw new Service('Injection calls with arguments must be passed in as `method`=>`params` array.');
					}

					foreach ($arguments[0] as $property => $value) {
						$this->_services[$class]->$property($value);
					}
				}

				return $this->_services[$class];
			}
			else {
				//asked for a service that was not annotated
				throw new Service("`{$class}Service` has not been injected. Annotation missing.");
			}
		}
		else {
			//not a Service call -- pass to parent
			return parent::__call($name, $arguments);
		}
	}

	/**
	 * Removes unnecessary properties for serialization
	 *
	 * @return array
	 */
	public function __sleep()
	{
		$ignoredProperties     = array(
			'_services',
			'_inspector'
		);
		$toSerializeProperties = array();

		foreach (get_object_vars($this) as $property => $value) {
			if (!in_array($property, $ignoredProperties)) {
				$toSerializeProperties[] = $property;
			}
		}

		return $toSerializeProperties;
	}

	/**
	 * Wake up function
	 */
	public function __wakeup()
	{
		if (!$this->_inspector) {
			$this->_inspector = new Inspector($this);
		}
		$meta = $this->_inspector->getClassMeta();

		if (isset($meta[Annotations::$service])) {
			foreach ($meta[Annotations::$service] as $service) {
				//Were not instantiating the service until its called
				$serviceParts                              = explode('\\', $service);
				$this->_services[array_pop($serviceParts)] = $service;
			}
		}
	}
}