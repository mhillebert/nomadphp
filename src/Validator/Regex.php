<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Validator;
/**
 * Class Regex
 *
 * @package Nomad\Validator
 * @author  Mark Hillebert
 */
class Regex
	extends AbstractValidator
{
	/**
	 * @static string Delimiter for regex if none provided.
	 */
	protected static $_delimiter = "~";

	/**
	 * @var string
	 */
	protected $_pattern;

	/**
	 * Useful for matching NOT.
	 * reverses the return, where normal if match found return true becomes if match found return false
	 * For example: passing this in an elements validators array:
	 * array('type' => 'Regex', 'pattern' => "[^a-zA-Z0-9\-_ ]", 'message' => 'This team name is currently unavailable.', 'failOnMatch' => true),
	 *
	 * says only allow a-zA-Z0-0 dashes underscores spaces
	 *
	 * @var bool
	 */
	protected $_failOnMatch = false;

	/**
	 * @param array $params
	 * @throws \Nomad\Exception\Form
	 */

	public function __construct(array $params)
	{
		if (!isset($params['pattern'])) {
			throw new \Nomad\Exception\Form('Pattern must be given. e.g: "pattern"=>"[a-zA-Z]"');
		}

		parent::__construct($params);
	}

	/**
	 * Checks $value for a non-decimal number (aka integer)
	 * Do not add delimiters at the start and end.
	 *
	 * @param       $value
	 * @param array $formValues
	 * @return bool|mixed
	 */
	public function isValid($value, $formValues = array())
	{
		$pattern = self::_preparePattern($this->_pattern);
		if (preg_match($pattern, $value)) {
			return !$this->_failOnMatch;
		}

		return $this->_failOnMatch;
	}

	/**
	 * Internally used helper function
	 *
	 * @param $pattern
	 * @return string
	 */
	protected function _preparePattern($pattern)
	{
		/**
		 * Check to see if delimiters were set by checking if the first and last characters are the same.
		 * If so, leave them, if not, add delimiters.
		 */
		//uses php 5.4+
		if ($pattern[0] == $pattern[strlen($pattern) - 1] && !ctype_alnum($pattern[0])) {
			return $pattern;
		}
		else {
			return self::$_delimiter . $pattern . self::$_delimiter;
		}
	}
}