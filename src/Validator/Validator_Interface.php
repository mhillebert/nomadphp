<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Validator;
/**
 * Interface Validator_Interface
 *
 * @package Nomad\Validator
 * @author  Mark Hillebert
 */
interface Validator_Interface
{
	/**
	 * @param       $value
	 * @param array $formValues
	 * @return mixed
	 */
	public function isValid($value, $formValues = array());

	/**
	 * @return mixed
	 */
	public function getMessage();
}
