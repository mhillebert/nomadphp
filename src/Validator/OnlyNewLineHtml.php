<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Validator;

/**
 * Class OnlyNewLineHtml
 *
 * @package Nomad\Validator
 * @author  Mark Hillebert
 */
class OnlyNewLineHtml
	extends AbstractValidator
{
	/**
	 * @var string
	 */
	protected $_filterStrength;

	/**
	 * @var array
	 */
	protected $_allowedTags = array('br');

	/**
	 * @var array
	 */
	protected $_allowedAttributes = array();

	/**
	 * @var string Message to display when validation fails
	 */
	protected $_message = "We are currently unable to accept this.";

	/**
	 * Filters valid html tags and no events
	 *
	 * @param       $value
	 * @param array $formValues
	 * @return bool|mixed
	 */
	public function isValid($value, $formValues = array())
	{
		if (empty($value)) {
			return true;
		}

		$config    = $this->_getConfiguration();
		$purifier  = new \HTMLPurifier($config);
		$converted = preg_replace("~\r\n~", '', nl2br($value));
		$filtered  = $purifier->purify($converted);
		if (strlen($converted) == strlen($filtered)) {

			return true;
		}

		return false;
	}

	/**
	 * Sets up the configuration for htmlpurifier
	 *
	 * @return \HTMLPurifier_Config
	 */
	protected function _getConfiguration()
	{
		$config = \HTMLPurifier_Config::createDefault();
		$config->set('HTML.Allowed', '');
		$config->set('HTML.AllowedAttributes', '');
		$config->set('HTML.AllowedElements', implode($this->_allowedTags, ','));

		return $config;
	}
}