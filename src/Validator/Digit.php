<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Validator;
/**
 * Class Digit
 *
 * @package Nomad\Validator
 * @author  Mark Hillebert
 */
class Digit
	extends AbstractValidator
{
	/**
	 * @var string
	 */
	public $_message = "Digits only please.";

	/**
	 * Checks $value for a non-decimal number (aka integer)
	 *
	 * @param       $value
	 * @param array $formValues
	 * @return bool|mixed
	 */
	public function isValid($value, $formValues = array())
	{
		if (empty($value)) {
			return true;
		}

		return ctype_digit($value);
	}
}