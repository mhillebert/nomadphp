<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Validator;

/**
 * Class AbstractValidator
 *
 * @package Nomad\Validator
 * @author  Mark Hillebert
 */
abstract class AbstractValidator
	implements Validator_Interface
{
	/**
	 * @var string Message to display when fail
	 */
	protected $_message;

	/**
	 * @var mixed
	 */
	protected $_params;

	/**
	 * @var bool Determine to stop processing validators if this fails
	 */
	protected $_breakChain = false;

	/**
	 * @param array $params OPTIONAL
	 * @throws \Nomad\Exception\Form
	 */
	public function __construct($params = null)
	{
		foreach ($params as $paramName => $value) {
			$protected = "_" . $paramName;
			if (\property_exists($this, $protected)) {
				$this->$protected = $value;
			}
		}
	}

	/**
	 * @param       $value
	 * @param array $formValues
	 * @return mixed
	 */
	abstract function isValid($value, $formValues = array());

	/**
	 * @return string
	 */
	public function getMessage()
	{
		return $this->_message;
	}

	/**
	 * @return bool
	 */
	public function willBreakChain()
	{
		return $this->_breakChain;
	}
}