<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Validator;
/**
 * Class IntegerLessThan
 *
 * @package Nomad\Validator
 * @author  Mark Hillebert
 */
class IntegerLessThan
	extends AbstractValidator
{
	/**
	 * @var mixed The value the validator is checking against
	 */
	protected $_value;

	/**
	 * @var string Message to display when fail
	 */
	public $_message = "Value was not a digit.";

	/**
	 * Checks $value for a non-decimal number (aka integer)
	 *
	 * @param       $value
	 * @param array $formValues
	 * @return bool|mixed
	 */
	public function isValid($value, $formValues = array())
	{
		if (!is_numeric($value)) {
			return false;
		}
		elseif (strpos($value, '.') !== false) {
			$this->_message = 'Please enter a whole number.';

			return false;
		}
		elseif ($value >= $this->_value) {
			$this->_message = "Must be less than {$this->_value}";

			return false;
		}

		return true;
	}
}