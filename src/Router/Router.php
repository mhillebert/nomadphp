<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Router;

use Nomad\Core as Core;
use Nomad\Exception as Exception;

/**
 * Class Router
 * The all important routing class
 *
 * @package Nomad\Core\Router
 * @author  Mark Hillebert
 */
class Router
	extends Core\BaseClass
{
	/**
	 * @var string Raw url from browser
	 */
	protected $_rawUrl;

	/**
	 * @var string Clean, usable url. Should only work with this url
	 */
	protected $_cleanUrl;

	/**
	 * @var array All known routes
	 */
	protected $_routes = array();

	/**
	 * @var Route The Route that matches the clean url
	 */
	protected $_matchedRoute;

	/**
	 * @param null $options
	 */
	public function __construct($options = null)
	{

		$options['inspector'] = false;
		parent::__construct($options);

		if ($this->getRawUrl()) {
			$this->_makeCleanUrl();
			$this->setMatchedRoute($this->matchRoute());
		}
	}

	/**
	 * Add a route for matching
	 *
	 * @param $name
	 * @param $route
	 * @return $this
	 */
	public function addRoute($name, $route)
	{
		$this->_routes[$name] = $route;

		return $this;
	}

	/**
	 * Not used by Nomad directly
	 * Return routes into pattern=>class array
	 *
	 * @return array
	 */
	public function getRoutes()
	{
		$routes = array();
		foreach ($this->_routes as $route) {
			$routes[$route->getPattern()] = get_class($route);
		}

		return $routes;
	}

	/**
	 * Return a route from the clean url
	 *
	 * @return Route|Route\Simple|null
	 */
	public function matchRoute()
	{
		$url = $this->_cleanUrl == '/' ? "/" : rtrim($this->_cleanUrl, '/'); // remove any tailing slashes

		foreach ($this->_routes as $route) {
			if ($route->matches($url)) {
				$this->_matchedRoute = $route;

				return $this->_matchedRoute;
			}
		}

		if (array_key_exists('404', $this->_routes)) {
			$namespace  = $this->_routes['404']->namespace;
			$action     = $this->_routes['404']->action;
			$parameters = null;

			$this->_matchedRoute = new \Nomad\Router\Route\Simple(
				array(
					'namespace' => $namespace,
					'action'    => $action,
					'params'    => $parameters
				));

			return $this->_matchedRoute;
		}

		return null;
		//$this->sendToDispatch('Common', $controller, $action, $parameters);

	}

	/**
	 * Return a Route by its name from config file
	 *
	 * @param $name
	 * @return null
	 */
	public function findRoute($name)
	{
		if (array_key_exists($name, $this->_routes)) {
			return $this->_routes[$name];
		}

		return null;
	}

	/**
	 * Cleans the raw url.
	 * Tries to combat hacks
	 */
	protected function _makeCleanUrl()
	{
		if (!empty($this->_rawUrl)) {
			//remove any colon matching variables
			$cleaner         = preg_replace("~[:;.*]~", '', $this->_rawUrl);
			$cleaner         = preg_replace("~[/]{2,}~", '/', $cleaner);
			$noNullBytes     = str_replace(chr(0), '', $cleaner);
			$this->_cleanUrl = filter_var($noNullBytes, FILTER_SANITIZE_URL);
		}
	}

	/**
	 * Stops Current Dispatch and Dispatches to new registered route name
	 * aka redirect.
	 *
	 * @param      $routeName
	 * @param null $params
	 */
	public function redirect($routeName = null, $params = null)
	{
		if (!$routeName) {
			$routeName = $this->_cleanUrl;
		}
		$route = $this->findRoute($routeName);
		if ($route) {
			$route->setParams($params);
		}

		Core\Registry::get('dispatcher')->stop();
		Core\Registry::get('dispatcher')->dispatch($route);
	}

	/**
	 * sends headers to redirect the page
	 * aka redirect.
	 *
	 * @param $routeName
	 */
	public function browserRedirect($routeName = null)
	{
		$pattern = $this->_cleanUrl;
		if (strpos($routeName, '/') !== false) {
			$pattern = $routeName;
		}
		elseif (!$routeName) {
		}
		else {
			$route   = $this->findRoute($routeName);
			$pattern = $route->getPattern();
		}
		\Nomad\Core\Registry::get('dispatcher')->setContinueToRenderer(false);

		header("Location: {$pattern}");
		exit;
	}

	/**
	 * Gets the current url (cleaned)
	 *
	 * @return string
	 */
	public function getCurrentUrl()
	{
		return $this->_cleanUrl;
	}

	/**
	 * Gets http_referer (last url)
	 *
	 * @return mixed
	 */
	public function getLastRoute()
	{
		if (isset($_SERVER['HTTP_REFERER'])) {
			return $_SERVER['HTTP_REFERER'];
		}
	}
}