<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Router\Route;

use Nomad\Router as Router;

/**
 * Class Regex
 * Regex Patterned Route
 *
 * @package Nomad\Router\Route
 * @author  Mark Hillebert
 */
class Regex
	extends Router\Route
{
	/**
	 * @var
	 */
	protected $_keys;

	/**
	 * @param $url
	 * @return bool
	 */
	public function matches($url)
	{
		$pattern = $this->pattern;

		//check values
		preg_match_all("~^{$pattern}$~", $url, $values);

		if (count($values) && count($values[0]) && count($values[1])) {
			//values found, modify params and return
			$derived           = array_combine($this->_keys, $values[1]);
			$this->_parameters = array_merge($this->_parameters, $derived);

			return true;
		}

		return false;
	}
}