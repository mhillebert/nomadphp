<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Router\Route;

use Nomad\Router as Router;
use Nomad\Unify\ArrayMethods as ArrayMethods;

/**
 * Class Simple
 *
 * @package Nomad\Router\Route
 * @author  Mark Hillebert
 */
class Simple
	extends Router\Route
{
	/**
	 * Determines if a url matches a Simple Route pattern
	 *
	 * @param $url
	 * @return bool|int
	 */
	public function matches($url)
	{
		$pattern = $this->getPattern();

		//get keys
		preg_match_all("~:([a-zA-Z0-9]+)~", $pattern, $keys);
		$result = $this->_matchWithNoParams($keys, $pattern, $url);
		if (!is_array($result)) {
			return preg_match("~^{$pattern}$~", $url);
		}
		else {
			$newKeys = $result;
		}

		//normalize route pattern
		$regexedPattern = preg_replace("~{?(:[a-zA-Z0-9]+)}?~", "([a-zA-Z0-9-_]+)", $pattern);

		//check values
		preg_match_all("~^{$regexedPattern}$~", $url, $values);

		if (sizeof($values) && sizeof($values[0]) && sizeof($values[1])) {
			//unset the matched url
			unset($values[0]);
			//values found, modify params and return
			$derived = array_combine($newKeys, ArrayMethods::flatten($values));
			$this->setParams(array_merge($this->_params, $derived));

			return true;
		}

		if (preg_match('{:[a-zA-Z0-9]+}', $pattern)) {
			//there are optional params, remove them and try match with no params again

			$removedOptional  = preg_replace('~\/{:[a-zA-Z0-9]+}~', '', $pattern);
			$noOptionalResult = $this->_matchWithNoParams(null, $removedOptional, $url);

			if (!is_array($noOptionalResult)) {
				return preg_match("~^{$removedOptional}$~", $url);
			}
		}

		return false;
	}

	/**
	 * Helper function for matches
	 *
	 * @param $keys
	 * @param $pattern
	 * @param $url
	 * @return int
	 */
	protected function _matchWithNoParams($keys, $pattern, $url)
	{
		if (sizeof($keys) && sizeof($keys[0]) && sizeof($keys[1])) {
			$keys = $keys[1];

			return $keys;
		}
		else {
			//No keys in pattern, return simple match
			return preg_match("~^{$pattern}$~", $url);
		}
	}
}