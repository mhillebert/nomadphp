<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Configuration\Driver;

use Nomad\Configuration as Configuration;
use Nomad\Unify\ArrayMethods as ArrayMethods;
use Nomad\Exception as Exception;

/**
 * Class Ini
 *
 * @package Nomad\Configuration\Driver
 * @author  Mark Hillebert
 */
class Ini
	extends Configuration\Driver
{
	/**
	 * Parse an ini file
	 *
	 * @param $path
	 * @return mixed
	 * @throws \Nomad\Exception\Argument
	 * @throws \Nomad\Exception\Syntax
	 */
	public function parse($path)
	{
		if (!$path) {
			throw new Exception\Argument("Cannot parse an empty path.");
		}
		if (!isset($this->_parsed[$path])) {
			$config   = array();
			$fileName = substr('.ini', -1, 4) == '.ini' ? $path : $path . '.ini';
			ob_start();
			include($path);
			$string = ob_get_contents();
			ob_end_clean();
			$pairs = parse_ini_string($string);
			if ($pairs == false) {
				throw new Exception\Syntax("Syntax Error in ini file. Could not parse.");
			}
			foreach ($pairs as $key => $value) {
				$config = $this->_pair($config, $key, $value);
			}
			$this->_parsed[$path] = ArrayMethods::toObject($config);
		}

		return $this->_parsed[$path];
	}

	/**
	 * Pairs ini configuration file into arrays from dot notation
	 *
	 * @param $config
	 * @param $key
	 * @param $value
	 * @return mixed
	 */
	protected function _pair($config, $key, $value)
	{
		if (strstr($key, ".")) {
			$parts = explode(".", $key, 2);
			if (empty($config[$parts[0]])) {
				$config[$parts[0]] = array();
			}
			$config[$parts[0]] = $this->_pair($config[$parts[0]], $parts[1], $value);
		}
		else {
			$config[$key] = $value;
		}

		return $config;
	}
}