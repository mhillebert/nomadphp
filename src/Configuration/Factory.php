<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Configuration;

use Nomad\Core as Core;
use Nomad\Exception as Exception;

/**
 * Class Factory
 *
 * @package Nomad\Configuration
 * @author  Mark Hillebert
 */
class Factory
	extends Core\BaseClass
{
	/**
	 * @var string Type of configuration to create
	 */
	protected $_type;

	/**
	 * @var mixed
	 */
	protected $_options;

	/**
	 * Create a configuration class based on type
	 *
	 * @param $type
	 * @return Driver\Ini|Driver\Yaml
	 * @throws \Nomad\Exception\Argument
	 */
	public static function create($type)
	{
		if (!$type) {
			throw new Exception\Argument("Configuration Driver Type is not set.");
		}
		switch ($type) {
			case "ini":
			case "Ini": {
				return new Driver\Ini();
				break;
			}
			case 'yml':
			case "yaml":
			case "Yaml": {
				return new Driver\Yaml();
				break;
			}
			default: {
				throw new Exception\Argument("Unknown Configuration Driver: {$type}.");
			}
		}
	}

	/**
	 * Instantiate the configuration class
	 *
	 * @return Driver\Ini|Driver\Yaml
	 * @throws \Nomad\Exception\Argument
	 */
	public function initialize()
	{
		if (!$this->_type) {
			throw new Exception\Argument("Configuration Driver Type is not set.");
		}
		switch ($this->_type) {
			case "ini":
			case "Ini": {
				return new Driver\Ini($this->_options);
				break;
			}
			case 'yml':
			case "yaml":
			case "Yaml": {
				return new Driver\Yaml($this->_options);
				break;
			}
			default: {
				throw new Exception\Argument("Unknown Configuration Driver: {$this->_type}.");
			}
		}
	}
}