# NomadPhp Framework Project

The Nomad Project's main goal is to provide a very quick and easy way for coders to design and develop a site. Nomad
is written in PHP with MySQL, jQuery, and other helper libraries to ensure the HTML5 works in older browsers.
The Nomad Project utilizes a blend of Hierarchical model-view-controller and Presentation-abstraction-control
architectural patterns. It is divided into interconnected parts which can significantly speed up development time.


This is designed to be a full-featured framework which includes:

* **Routing** - Both simple and dynamic are handled.
* **Annotations** - Use annotations to simplify tasks like dependency injection, acl checks, change layouts, and more.
* **Registry** - Use a registry for things.
* **Sessions** - A simple and elegant way to interact with php sessions.
* **Caching** - Cache whole pages, partials (blocks) and database queries
* **Forms** - Create forms with validation and filtering with many predefined element types (or define your own). Use 
form success and failure handlers to simplify form processing. Plus auto-csrf (disable-able).
* **View Rendering** - Anything can become 'Viewable' and placed within the specified layout and/or theme.
* **Themes** - Create themes easily by defining some placeholders, css, and js
* **Database** - Easily make calls to a database (only mySQL at this time, but you can add your own driver to use whatever).
* **Models** - Populate models easily from a database. Complete with property filtering via annotations.
* **Email** - Send html emails easily by setting up a theme and a view.
* **Access Control** - Define roles and oder them in a tree. Then allow annotations to determine access.
* **Fine Grain Access Control** - Define custom functions to check before allowing access.
* **XCss** - Php within css files simplify asset url creation, allow variables for things such as color. Complete with 
helper functions to shade and rotate colors. These files are interpreted and then placed in the public folder. Note: 
Interpretation only occurs on DEVELOPMENT environments.
* **Static Helpers** - Array methods such as flatten, String methods such as makePath, Date methods such as MySQL formatting.
* **Validators** - Many common validators such as digit, email address, no html, and more.
* **Filters** - Many common filters such as restricted html, safe string trim, and more.

This project does incorporate other packages: RatchetPhp for web sockets, html purifier, kint debugger.

## Getting Started
NomadPhp comes with the website as an example to make the learning curve a little easier. I could have made a separate
repository that sets up the default directories within your project similar to symfony; however I only wanted to maintain 
a single repository. To make this happen use this **composer.json** file:

```javascript
{
  "name": "yourCompany/yourProject",
  "description": "Your project's description",
  "require": {
    "mhillebert/nomad": "dev-master"
  },
  "type" : "project",
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://mhillebert@bitbucket.org/mhillebert/nomad.git"
    }
  ],
  "autoload": {
    "psr-4": {
      "Nomad\\": "vendor/mhillebert/nomad/src"
    }
  },
  "authors": [
    {
      "name": "Your name",
      "email": "your@email"
    }
  ],
  "scripts": {
      "post-install-cmd": [
          "mv vendor/mhillebert/nomad/Application/ .",
          "mv vendor/mhillebert/nomad/bin/ .",
          "mv vendor/mhillebert/nomad/cron/ .",
          "mv vendor/mhillebert/nomad/cache/ .",
          "mv vendor/mhillebert/nomad/onetime/ .",
          "mv vendor/mhillebert/nomad/tests/ .",
          "mv vendor/mhillebert/nomad/tmp/ .",
          "mv vendor/mhillebert/nomad/uploads/ .",
          "mv vendor/mhillebert/nomad/public/ .",
          "cp -r vendor/mhillebert/nomad/vendor/mhillebert/ vendor",
          "rm -rf vendor/mhillebert/nomad/vendor"
      ]
  }
}
```

This will install all the necessary dependencies and set you up right. The only other configuration is your web server.
If you use apache then you should include (replace 'development' with your server's environment) 

>setEnv ENVIRONMENT=development

## On Your Way
You should be able to access your project's site at this time. It will be the framework's static website [Nomad Project](http://thenomadproject.net).
The documentation at this time is not complete and youmost likely will need to dive into the source code. 
But, no worries, this is what learning code is all about. So, play around, extend classes and break it, then make it better! 

